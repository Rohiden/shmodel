// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('olafApp', ['ionic', 'angular-momentjs', 'ngTable'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });

    })

    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $httpProvider.defaults.withCredentials = true;

        //   $httpProvider.defaults.headers.common.Authorization = 'Basic YWRtaW46YWRtaW4=';

        //     $httpProvider.defaults.headers.post['Accept'] = 'application/json; q=0.01';
        // $httpProvider.defaults.headers.post['X-Requested-With'] = 'XMLHttpRequest';

        //    delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $stateProvider
            .state('start', {
                url: '/start',
                templateUrl: 'templates/start.html'
            })
            .state('home', {
                url: '/home',
                templateUrl: 'templates/home.html'
            })
            .state('userProgress', {
                url: '/userProgress/:userId',
                templateUrl: 'templates/user/userProgress.html'
            })
            .state('newUser', {
                url: '/newUser/:userId',
                templateUrl: 'templates/user/newUser.html'
            })
            .state('category', {
                url: '/category',
                templateUrl: 'templates/category/category.html'
            })

            .state('subcategory', {
                url: '/subcategory/:id',
                templateUrl: 'templates/category/subcategory.html'
            })
            .state('actionType', {
                url: '/actionType/:categoryId/:subcategoryId',
                templateUrl: 'templates/actions/actionType.html'
            })
            .state('gameAction', {
                url: '/gameAction/:actionType/:categoryId/:subcategoryId',
                templateUrl: 'templates/actions/gameAction.html'
            })
            .state('wordLearn', {
                url: 'wordLearn/:actionType/:categoryId/:subcategoryId',
                templateUrl: 'templates/games/words/wordLearn.html'
            })
            .state('wordExercise', {
                url: 'wordExercise/:actionType/:categoryId/:subcategoryId',
                templateUrl: 'templates/games/words/wordExercise.html'
            })
            .state('wordSpeach', {
                url: 'wordSpeach/:actionType/:categoryId/:subcategoryId',
                templateUrl: 'templates/games/words/wordSpeach.html'
            })
            .state('wordMemory', {
                url: 'wordMemory/:actionType/:categoryId/:subcategoryId',
                templateUrl: 'templates/games/words/wordMemory.html'
            })
            .state('expressionLearn', {
                url: 'expressionLearn/:actionType/:categoryId/:subcategoryId',
                templateUrl: 'templates/games/expressions/expressionLearn.html'
            })
            .state('expressionExercise', {
                url: 'expressionExercise/:actionType/:categoryId/:subcategoryId',
                templateUrl: 'templates/games/expressions/expressionExercise.html'
            })
            .state('expressionSpeach', {
                url: 'expressionSpeach/:actionType/:categoryId/:subcategoryId',
                templateUrl: 'templates/games/expressions/expressionSpeach.html'
            })
            .state('expressionMemory', {
                url: 'expressionMemory/:actionType/:categoryId/:subcategoryId',
                templateUrl: 'templates/games/expressions/expressionMemory.html'
            })
            .state('singup', {
                url: 'keeper/singup',
                templateUrl: 'templates/keeper/singup.html'
            })
            .state('manage', {
                url: 'keeper/manage',
                templateUrl: 'templates/keeper/manage.html'
            });
        $urlRouterProvider.otherwise('/start');




        var httpInterceptor = ['$q','$injector', function($q,$injector) {
        // {function ($q) {

            return {
                response: function (response) { return response; },
                responseError: function (response) {
                    var status = response.status;

                    if (status == 401) {
                                   var stateService = $injector.get('$state');
                        stateService.go('start');
                        return;
                    }
                    return $q.reject(response);
                }
            };


        }];
        $httpProvider.interceptors.push(httpInterceptor);


    });
