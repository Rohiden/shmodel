(function () {
    'use strict';

    angular.module('olafApp').controller('UserProgressCtrl', [
        '$stateParams', '$ionicHistory', '$state', 'UserFactory', 'UserStatsFactory', '$moment', 'NgTableParams',
        function ($stateParams, $ionicHistory, $state, UserFactory, UserStatsFactory, $moment, NgTableParams) {
            var vm = this;
            var userId = Number($stateParams.userId);
            vm.userActivities = [];
            var userStats = [];
            var itemToLoad = 10;
            vm.goBack = function () {
                $ionicHistory.goBack();
            }

            UserFactory.getUsers().then(function (response) {
                var user = _.find(response, function (item) {
                    return item.Id == userId;
                });
                user.LastLoginDate = $moment(user.LastLogin).format('YYYY-MM-DD HH:mm:ss')
                vm.selectedUser = user;
            });


            UserStatsFactory.getUserStats(userId).then(function (response) {
                _.forEach(response, function (item) {

                    item.Date = $moment(item.StartGame).format('YYYY-MM-DD HH:mm:ss') + ' - ' + (item.EndGame !== null ? $moment(item.EndGame).format('HH:mm:ss') : 'Nie ukończono');
                    item.Time = item.IsSuccess ? '(' + $moment(item.EndGame).diff($moment(item.StartGame), 'seconds') + 's)' : ''

                    userStats.push(item);
                });
                vm.userActivities = _.take(userStats, itemToLoad);
                preperDateForCharts(response);
            });

            vm.loadMore = function () {
                itemToLoad += 10;
                vm.userActivities = _.take(userStats, itemToLoad);
            }

            function preperDateForCharts(userStats) {
                var groupedStats = _.groupBy(userStats, function (item) {
                    return item.GameName;
                });
                var counter = 0;
                var container = {
                    words: [],
                    expressions: [],
                };
                var series = [];
                _.forIn(groupedStats, function (value, key) {

                    var itemSeries = {}
                    itemSeries.name = key;
                    itemSeries.data = [];
                    _.forEach(value, function (entity) {
                        var time;
                        if (entity.IsSuccess) {
                            time = $moment(entity.EndGame).diff($moment(entity.StartGame), 'seconds');
                            itemSeries.data.push([$moment(entity.StartGame).valueOf(), time]);
                        }
                    })
                    if (key.toLowerCase().indexOf('wypowiedź') !== -1) {
                        container.expressions.push(itemSeries);
                    } else {
                        container.words.push(itemSeries);
                    }

                    counter += 1;
                });
                handleChartsSeries(container);
            }

            function handleChartsSeries(container) {
                generateChart('wordContainer', container.words);
                generateChart('expressionContainer', container.expressions);
                generatePieCharts();
            }

            function generatePieCharts() {
                var gameNames = ["Wypowiedź - Mowa", "Wypowiedź - Ćwiczę", "Wypowiedź - Ucze się", "Wypowiedź - Pamięć", "Słowa - Ćwiczę", "Słowa - Ucze się", "Słowa - Pamięć", "Słowa - Mowa"];
                var counter = 0;
                var allGames = _.groupBy(userStats, 'GameName');

                _.forEach(gameNames, function (game) {
                    var itemsPerGame = allGames[game];
                    if(itemsPerGame == undefined || itemsPerGame.length == 0){return;}
                    var allFinished = _.groupBy(itemsPerGame, 'IsSuccess')
                    var doneItems = allFinished[true] != undefined ? allFinished[true].length : 0 ;

                    $('#container' + counter).highcharts({
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: game
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            colorByPoint: true,
                            data: [{
                                name: 'Ukończonych',
                                y: (doneItems / itemsPerGame.length) * 100
            }, {
                                name: 'Przerwanych',
                                y: ((itemsPerGame.length - doneItems) / itemsPerGame.length) * 100,
                                sliced: true,
                                selected: true
            }]
        }]
                    });

                    counter += 1;
                })



            }

            function generateChart(containerId, series) {
                $('#' + containerId).highcharts({
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: 'Postęp'
                    },
                    xAxis: {
                        type: 'datetime',
                        dateTimeLabelFormats: { // don't display the dummy year
                            month: '%e. %b',
                            year: '%b'
                        },
                        title: {
                            text: 'Data'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Czas [s]'
                        },
                        min: 0
                    },
                    plotOptions: {
                        spline: {
                            marker: {
                                enabled: true
                            }
                        }
                    },
                    series: series,
                });
            }

        }]);
})();