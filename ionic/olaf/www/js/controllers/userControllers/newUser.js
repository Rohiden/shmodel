(function () {
    'use strict';

    angular.module('olafApp').controller('NewUserCtrl', [
        '$stateParams', '$ionicHistory', '$state', 'UserFactory', 'UserStatsFactory', '$moment', 'NgTableParams', '$ionicPopup', '$timeout',
        function ($stateParams, $ionicHistory, $state, UserFactory, UserStatsFactory, $moment, NgTableParams, $ionicPopup, $timeout) {
            var vm = this;
            vm.Login = '';
            vm.NameFirst = '';
            vm.NameLast = '';
            vm.saving = false;
            vm.Id = -1;
            var userId = Number($stateParams.userId);

            if (userId !== 0) {
                UserFactory.getUser(userId).then(function (response) {
                    vm.Login = response.Login;
                    vm.NameFirst = response.NameFirst;
                    vm.NameLast = response.NameLast;
                    vm.Id = response.Id;
                })
            }

            vm.saveUser = function () {
                vm.saving = true;
                var userModel = {
                    Login: vm.Login,
                    NameFirst: vm.NameFirst,
                    NameLast: vm.NameLast,
                    Id: vm.Id
                };
                UserFactory.addUser(userModel).then(function (response) {
                    vm.saving = false;
                    if (response.status === 200) {
                        showAlert(true);
                    } else {
                        showAlert(false);
                    }
                });
            };

            function showAlert(isSuccess) {
                var title = isSuccess ? 'Zapisano' : 'Wystąpił error';
                var alertPopup = $ionicPopup.alert({
                    title: title
                });
                $timeout(function () {
                    alertPopup.close();
                    $state.go('home');
                }, 1000);
            }
        }]);
})();