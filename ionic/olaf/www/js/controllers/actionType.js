(function () {
    'use strict';

    angular.module('olafApp').controller('actionTypeCtrl', [
        'olafApi', '$stateParams', '$ionicHistory', '$state','SettingFactory',
        function (olafApi, $stateParams, $ionicHistory, $state,SettingFactory) {
            var vm = this;

            var categoryId = Number($stateParams.categoryId);
            var subcategoryId = Number($stateParams.subcategoryId);
            var actionTypeId = Number($stateParams.actionType);

            vm.selectedAction = function (type) {
                SettingFactory.setGameType(type);
                $state.go('gameAction', {
                    actionType: type,
                    categoryId: categoryId,
                    subcategoryId: subcategoryId
                });
            };
            
            vm.baseURL = SettingFactory.getHostURL();

            vm.goBack = function () {
                $ionicHistory.goBack();
            };

        }]);
})();