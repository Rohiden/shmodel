(function () {
    'use strict';

    angular.module('olafApp').controller('expressionExerciseCtor', [
        'olafApi', '$stateParams', '$ionicHistory', '$state', '$timeout', 'SubcategoryFactory', 'ActionFactory', 'NounFactory', 'PersonFactory', 'UserStatsFactory', 'SettingFactory',
        function (olafApi, $stateParams, $ionicHistory, $state, $timeout, SubcategoryFactory, ActionFactory, NounFactory, PersonFactory, UserStatsFactory, SettingFactory) {
            var vm = this;
            vm.nouns = [];
            vm.person = {};
            vm.isWin = false;
            vm.showItem = 0;
            vm.subtitleVisible = SettingFactory.checkIsLiteral();
            vm.showBig = [true, true, true];
            vm.showBigImage = [false, false, false];
            var categoryId = Number($stateParams.categoryId);
            var subcategoryId = Number($stateParams.subcategoryId);
            var actionSound = {};
            var personSound = {};
            var selectedItems = [];
            var counter = 0;
            doneItem = undefined;
            vm.baseURL = SettingFactory.getHostURL();

            ActionFactory.getValue(subcategoryId).then(function (response) {
                var actionItem = response;
                vm.Action = actionItem;
                actionSound = new Howl({
                    urls: [actionItem.Sound],
                });
            });

            NounFactory.getNouns(subcategoryId).then(function (response) {
                _.forEach(response, function (item) {
                    sounds[counter] = new Howl({
                        urls: [item.Sound],
                        onplay: checkIfWin,
                    });
                    vm.nouns.push(item);
                    counter += 1;
                });
                counter = 0;
                UserStatsFactory.newGame('Wypowiedź - Ćwiczę', subcategoryId);

            });


            NounFactory.getNouns(0).then(function (response) {
                vm.fakeNouns = [];
                _.forEach(response, function (item) {
                    vm.fakeNouns.push(item);
                    counter += 1;
                });
                counter = 0;
            });

            PersonFactory.getPerson(subcategoryId).then(function (response) {
                _.forEach(response, function (item) {
                    personSound = new Howl({
                        urls: [item.Sound]
                    });
                    vm.person = item;
                });
            });


            vm.nounClickHandle = function (index) {
                if (_.indexOf(vm.showBigImage, false) === -1) {
                    vm.showBig = [false, false, false]
                    return;
                }
                sounds[index].play();
                vm.showBigImage[index] = true;
            }

            vm.clickActionImage = function () {
                actionSound.play();
                vm.showActionImage = true;
                vm.showPersonContainer = true;
            }

            vm.clickOnPersonContainer = function () {
                personSound.play();
            }

            function checkIfWin() {
                if (doneItem !== undefined) {
                    $timeout(function () {

                        vm.showBig[doneItem] = true;
                        if (_.indexOf(vm.showBig, false) === -1) {
                            vm.isWin = true;
                            UserStatsFactory.gameFinished();
                        }
                    }, 100);
                }
            }

            vm.goBack = function () {
                $ionicHistory.goBack();
            };
            handleInteractiv();

        }]);

    function handleInteractiv() {
        handelDraggingElements('#noun0-0');
        handelDraggingElements('#noun1-0');
        handelDraggingElements('#noun0-1');
        handelDraggingElements('#noun1-1');
        handelDraggingElements('#noun0-2');
        handelDraggingElements('#noun1-2');
        handleDropzone('#noun0-0, #noun0-1, #noun0-2');
    }

    function handelDraggingElements(elementId) {
        interact(elementId)
            .draggable({
                onmove: function (event) {
                    var target = event.target;
                    target.x = (target.x | 0) + event.dx;
                    target.y = (target.y | 0) + event.dy;
                    target.style.webkitTransform = target.style.transform =
                        'translate(' + target.x + 'px, ' + target.y + 'px)';
                    console.log('terefer');
                },
                onstart: function (event) {
                    startX = event.dx;
                    startY = event.dy;
                    $(".person-item-img").addClass('movedElement');
                    $('.nounBigContent').children().not(elementId).addClass('movedElement');
                },
                onend: function (event) {
                    console.log('end');
                    var target = event.target;
                    target.x = startX;
                    target.y = startY;
                    target.style.webkitTransform = target.style.transform =
                        'translate(' + startX + 'px, ' + startY + 'px)';
                    $('.nounBigContent').children().removeClass('movedElement');
                }

            })
            .inertia(true);
    }

    function handleDropzone(elementId) {
        interact('.dropzone')
            // enable draggables to be dropped into this
            .dropzone(true)
            // only accept elements matching this CSS selector
            .accept(elementId)
            // listen for drop related events
            .on('dragenter', function (event) {
                var draggableElement = event.relatedTarget,
                    dropzoneElement = event.target;

                // feedback the possibility of a drop
                dropzoneElement.classList.add('drop-target');
                draggableElement.classList.add('can-drop');
            })
            .on('dragleave', function (event) {
                // remove the drop feedback style
                event.target.classList.remove('drop-target');
                event.relatedTarget.classList.remove('can-drop');
            })
            .on('drop', function (event) {
                var draggableElement = event.relatedTarget,
                    dropzoneElement = event.target;
                var itemId = $(draggableElement).attr('id');
                unsetElement(itemId);

                doneItem = parseInt(itemId.split('-')[1]);

                //
                //                        $('#left-containter-' + elementId).css('visibility', 'hidden');
                //                        $('.middle-item-' + elementId).css('visibility', 'visible');
                //                        finishedItems.push(elementId);
                sounds[doneItem].play();
            });
    }

    function unsetElement(elementId) {
        interact('#' + elementId).unset();
    }

    var startX, startY, doneItem;

    var sounds = [];





})();
