(function () {
    'use strict';

    angular.module('olafApp').controller('expressionSpeachCtor', [
        'olafApi','SettingFactory', '$stateParams', '$ionicHistory', '$state', '$timeout', 'NounFactory', 'ActionFactory', 'AnnyangService', 'PersonFactory', '$q', 'UserStatsFactory','$scope',
        function (olafApi,SettingFactory, $stateParams, $ionicHistory, $state, $timeout, NounFactory, ActionFactory, AnnyangService, PersonFactory, $q, UserStatsFactory,$scope) {
            var vm = this;
            vm.nouns = [];
            vm.selectedItem = {};
            vm.isWin = false;
            var categoryId = Number($stateParams.categoryId);
            var subcategoryId = Number($stateParams.subcategoryId);
            var sounds = [];
            var actionSound = {};
            var personSound = {};
            var selectedItems = [];
            vm.showNoum = 0;
            vm.baseURL = SettingFactory.getHostURL();
            var nounPromice = NounFactory.getNouns(subcategoryId).then(function (response) {
                var counter = 0;
                _.forEach(response, function (item) {
                    sounds[counter] = new Howl({
                        urls: [item.Sound]
                    });
                    item.completed = '';
                    vm.nouns.push(item);

                    counter += 1;
                });
                vm.showNoum = 0;
            });

            var actionPromice = ActionFactory.getValue(subcategoryId).then(function (response) {
                var actionItem = response;
                vm.action = actionItem;
                actionSound = new Howl({
                    urls: [actionItem.Sound],
                });
            });

            var personPromice = PersonFactory.getPerson(subcategoryId).then(function (response) {
                _.forEach(response, function (item) {
                    vm.person = item;
                    personSound = new Howl({
                      urls: [item.Sound],
                    })
                });
            });

            $q.all([nounPromice, actionPromice, personPromice]).then(function () {
                generateCommand();
                AnnyangService.start();
                UserStatsFactory.newGame('Wypowiedź - Mowa', subcategoryId);
            })

            function generateCommand() {
                //                var firstCommand = vm.person.Name.Text.trim() + ' ' + vm.action.Text + ' ' + vm.nouns[vm.showNoum].Singular.Text;
                var firstCommand = vm.person.Text.trim() + ' ' + vm.action.Text.trim() + ' ' + vm.nouns[vm.showNoum].Text.trim();
                addCommand(firstCommand);
            }


            vm.clickOnImage = function (index) {
                sounds[index].play();
            };

            vm.actionClick = function(){
                actionSound.play();
            }
            vm.personClick = function(){
                personSound.play();
            }
            function addCommand(name) {
                AnnyangService.addCommand(name, function () {
                    if (vm.showNoum >= vm.nouns.length - 1) {
                        vm.isWin = true;
                        UserStatsFactory.gameFinished();
                    } else {
                        vm.showNoum += 1;
                        generateCommand();
                    }
                })
            };

            vm.goBack = function () {
                AnnyangService.abort();
                $ionicHistory.goBack();
            };

            $scope.$on('$ionicView.beforeLeave', function () {
                AnnyangService.abort();
            });

        }]);
})();
