(function () {
    'use strict';

    angular.module('olafApp').controller('expressionLearnCtor', [
        'olafApi', '$stateParams', '$ionicHistory', '$state', '$timeout', 'SubcategoryFactory', 'ActionFactory', 'NounFactory', 'PersonFactory', 'UserStatsFactory', 'SettingFactory',
        function (olafApi, $stateParams, $ionicHistory, $state, $timeout, SubcategoryFactory, ActionFactory, NounFactory, PersonFactory, UserStatsFactory, SettingFactory) {
            var vm = this;
            vm.nouns = [];
            vm.person = {};
            vm.isWin = false;
            vm.showItem = 0;
            vm.subtitleVisible = SettingFactory.checkIsLiteral();
            vm.showActionImage = false;
            vm.showPersonImage = false;
            vm.showPersonContainer = false;
            vm.showNounImg = false;
            var categoryId = Number($stateParams.categoryId);
            var subcategoryId = Number($stateParams.subcategoryId);
            var sounds = [];
            var actionSound = {};
            var personSound = {};
            var selectedItems = [];
            var counter = 0;
            var nounCounter = 0;
            vm.baseURL = SettingFactory.getHostURL();

            ActionFactory.getValue(subcategoryId).then(function (response) {
                var actionItem = response;
                vm.Action = actionItem;
                actionSound = new Howl({
                    urls: [actionItem.Sound],
                });
            });
            NounFactory.getNouns(subcategoryId).then(function (response) {
                _.forEach(response, function (item) {
                    sounds[counter] = new Howl({
                        urls: [item.Sound],
                        onend: checkIfWin
                    });
                    vm.nouns.push(item);
                    counter += 1;
                });
                counter = 0;
                UserStatsFactory.newGame('Wypowiedź - Ucze się', subcategoryId);
            });

            PersonFactory.getPerson(subcategoryId).then(function (response) {
                _.forEach(response, function (item) {
                    personSound = new Howl({
                        urls: [item.Sound]
                    });
                    vm.person = item;
                });
            });


            vm.nounClickHandle = function () {
                nounCounter += 1;
                if (vm.showItem < 2 && nounCounter > 1) {
                    vm.showItem += 1;
                }
                vm.showNounImg = true;
                sounds[vm.showItem].play();



            }

            vm.clickActionImage = function () {
                actionSound.play();
                vm.showActionImage = true;
                vm.showPersonContainer = true;
            }

            vm.clickOnPersonContainer = function () {
                vm.showPersonImage = true;
                personSound.play();
            }

            function checkIfWin() {
                if (nounCounter > 2) {

                    UserStatsFactory.gameFinished();
                    vm.isWin = true;
                }
            }

            vm.goBack = function () {
                $ionicHistory.goBack();
            };

        }]);
})();
