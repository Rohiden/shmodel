(function () {
    'use strict';

    angular.module('olafApp').controller('wordExerciseCtor', [
        'olafApi', '$stateParams', '$ionicHistory', '$state', '$timeout','UserStatsFactory','NounFactory','SettingFactory',
        function (olafApi, $stateParams, $ionicHistory, $state, $timeout,UserStatsFactory,NounFactory,SettingFactory) {
            var vm = this;
            vm.nouns = [];
            vm.isWin = false;
            var categoryId = Number($stateParams.categoryId);
            var subcategoryId = Number($stateParams.subcategoryId);
            items = [];
            finishedItems = [];
            var selectedItems = [];
            vm.baseURL = SettingFactory.getHostURL();
            vm.isLiteral = SettingFactory.checkIsLiteral();

            NounFactory.getNouns(subcategoryId).then(function (response) {
                _.forEach(response, function (item) {
                    sounds[item.Id] = new Howl({
                        urls: [item.Sound],
                        onend: checkIfWin
                    });
                    vm.nouns.push(item);
                    items.push(item);
                });
                vm.randElements = randElements(items);
                handleInteractiv(vm);
                UserStatsFactory.newGame('Słowa - Ćwiczę', subcategoryId);
            });

            function randElements(array) {
                var currentIndex = array.length,
                    temporaryValue, randomIndex;

                // While there remain elements to shuffle...
                while (0 !== currentIndex) {

                    // Pick a remaining element...
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;

                    // And swap it with the current element.
                    temporaryValue = array[currentIndex];
                    array[currentIndex] = array[randomIndex];
                    array[randomIndex] = temporaryValue;
                }

                return array;
            }

            function checkIfWin() {
                if (finishedItems.length > 2) {
                    $timeout(function () {
                        vm.isWin = true;
                        UserStatsFactory.gameFinished();
                    }, 100);
                }
            }

            vm.goBack = function () {
                $ionicHistory.goBack();
            };

        }]);
    var items = [];

    function handleInteractiv() {
        _.forEach(items, function (item) {
            handleDropzone(item.Id);
            handelDraggingElements(item.Id);
        });
    }

    function handelDraggingElements(elementId) {
        interact('#image' + elementId)
            .draggable({
                onmove: function (event) {
                    var target = event.target;
                    target.x = (target.x | 0) + event.dx;
                    target.y = (target.y | 0) + event.dy;
                    target.style.webkitTransform = target.style.transform =
                        'translate(' + target.x + 'px, ' + target.y + 'px)';
                    console.log('terefer');
                },
                onstart: function (event) {
                    startX = event.dx;
                    startY = event.dy;
                },
                onend: function (event) {
                    if (!interact.isSet('#image' + elementId)) {
                        return;
                    }
                    console.log('end');
                    var target = event.target;
                    target.x = startX;
                    target.y = startY;
                    target.style.webkitTransform = target.style.transform =
                        'translate(' + startX + 'px, ' + startY + 'px)';
                }

            })
            .inertia(true);
    }

    function handleDropzone(elementId) {
        interact('.dropzone' + elementId)
            // enable draggables to be dropped into this
            .dropzone(true)
            // only accept elements matching this CSS selector
            .accept('#image' + elementId)
            // listen for drop related events
            .on('dragenter', function (event) {
                var draggableElement = event.relatedTarget,
                    dropzoneElement = event.target;

                // feedback the possibility of a drop
                dropzoneElement.classList.add('drop-target');
                draggableElement.classList.add('can-drop');
            })
            .on('dragleave', function (event) {
                // remove the drop feedback style
                event.target.classList.remove('drop-target');
                event.relatedTarget.classList.remove('can-drop');
            })
            .on('drop', function (event) {
                unsetElement(elementId);
                $('#left-containter-' + elementId).css('visibility', 'hidden');
                $('.middle-item-' + elementId).css('visibility', 'visible');
                finishedItems.push(elementId);
                sounds[elementId].play();
            });
    }

    function unsetElement(elementId) {
        interact('#image' + elementId).unset();
        interact('.dropzone' + elementId).unset();
    }
    var startX, startY;
    var finishedItems = [];
    var sounds = [];
})();
