(function () {
    'use strict';

    angular.module('olafApp').controller('wordMemoryCtor', [
        'olafApi', '$stateParams', '$ionicHistory', '$state', '$timeout', 'UserStatsFactory','NounFactory','SettingFactory',
        function (olafApi, $stateParams, $ionicHistory, $state, $timeout, UserStatsFactory,NounFactory,SettingFactory) {
            var vm = this;
            vm.selectedItem = {};
            vm.isWin = false;
            var categoryId = Number($stateParams.categoryId);
            var subcategoryId = Number($stateParams.subcategoryId);
            var sounds = [];
            var selectedItems = [];
            vm.nouns = [];
            var showedItems = [];

            vm.baseURL = SettingFactory.getHostURL();
            vm.isLiteral = SettingFactory.checkIsLiteral();

            NounFactory.getNouns(subcategoryId).then(function (response) {
                var nouns = [];
                _.forEach(response, function (item) {
                    sounds[item.Id] = new Howl({
                        urls: [item.Sound]
                    });
                    item.showItem = false;
                    nouns.push(item);
                    nouns.push(_.clone(item));
                });
                var items = randElements(nouns);
                vm.nouns[0] = _.take(items, 3);
                vm.nouns[1] = _.takeRight(items, 3);
                UserStatsFactory.newGame('Słowa - Pamięć', subcategoryId);
            });

            vm.clickOnImage = function (noun) {
                sounds[noun.Id].play();
                if(noun.showItem) return;
                noun.showItem = true;

                if (showedItems.length === 0) {
                    showedItems.push(noun);
                } else {
                    if (showedItems[0].Id === noun.Id) {
                        showedItems = [];
                        var isWin = true;
                        _.forEach(vm.nouns, function (row) {
                            _.forEach(row, function (item) {
                                if (item.showItem === false) {
                                    isWin = false;
                                }
                            });
                        });
                        $timeout(function () {
                            if (isWin) {
                                UserStatsFactory.gameFinished();
                                vm.isWin = isWin;
                            }
                        }, 1500);
                    } else {
                        $timeout(function () {
                            noun.showItem = false;
                            showedItems[0].showItem = false;
                            showedItems = [];
                        }, 1500);
                    }
                }

            }

            function randElements(array) {
                var currentIndex = array.length,
                    temporaryValue, randomIndex;

                // While there remain elements to shuffle...
                while (0 !== currentIndex) {

                    // Pick a remaining element...
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;

                    // And swap it with the current element.
                    temporaryValue = array[currentIndex];
                    array[currentIndex] = array[randomIndex];
                    array[randomIndex] = temporaryValue;
                }

                return array;
            }

            vm.goBack = function () {
                $ionicHistory.goBack();
            };

        }]);
})();
