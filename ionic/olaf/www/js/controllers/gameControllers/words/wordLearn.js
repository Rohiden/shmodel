(function () {
    'use strict';

    angular.module('olafApp').controller('wordLearnCtor', [
        'olafApi', '$stateParams', '$ionicHistory', '$state', '$timeout', 'UserStatsFactory', 'NounFactory', 'SettingFactory',
        function (olafApi, $stateParams, $ionicHistory, $state, $timeout, UserStatsFactory, NounFactory, SettingFactory) {
            var vm = this;
            vm.nouns = [];
            vm.selectedItem = {};
            vm.isWin = false;
            var categoryId = Number($stateParams.categoryId);
            var subcategoryId = Number($stateParams.subcategoryId);
            var sounds = [];
            var selectedItems = [];

            vm.baseURL = SettingFactory.getHostURL();
            vm.isLiteral = SettingFactory.checkIsLiteral();

            NounFactory.getNouns(subcategoryId).then(function (response) {
                _.forEach(response, function (item) {
                    sounds[item.Id] = new Howl({
                        urls: [item.Sound],
                        onend: checkIfWin
                    });
                    vm.nouns.push(item);
                });
                UserStatsFactory.newGame('Słowa - Ucze się', subcategoryId);
            });


            vm.clickOnSmallImage = function (item) {
                vm.selectedItem = item;
                if (selectedItems.indexOf(item.Id) === -1) {
                    selectedItems.push(item.Id);
                }
                sounds[item.Id].play();
            };

            function checkIfWin() {
                if (selectedItems.length > 2) {
                    $timeout(function () {
                        vm.isWin = true;
                        UserStatsFactory.gameFinished();
                    }, 100);
                }
            }

            vm.goBack = function () {
                $ionicHistory.goBack();
            };

        }]);
})();
