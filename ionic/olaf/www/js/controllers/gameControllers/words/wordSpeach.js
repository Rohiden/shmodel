(function () {
    'use strict';

    angular.module('olafApp').controller('wordSpeachCtor', [
        'olafApi', '$stateParams', '$ionicHistory', '$state', '$timeout', 'NounFactory', 'AnnyangService', 'UserStatsFactory', 'SettingFactory','$scope',
        function (olafApi, $stateParams, $ionicHistory, $state, $timeout, NounFactory, AnnyangService, UserStatsFactory, SettingFactory,$scope) {
            var vm = this;
            vm.nouns = [];
            vm.selectedItem = {};
            vm.isWin = false;
            var categoryId = Number($stateParams.categoryId);
            var subcategoryId = Number($stateParams.subcategoryId);
            var sounds = [];
            var selectedItems = [];

            vm.baseURL = SettingFactory.getHostURL();
            vm.isLiteral = SettingFactory.checkIsLiteral();

            NounFactory.getNouns(subcategoryId).then(function (response) {
                var counter = 0;
                _.forEach(response, function (item) {
                    sounds[counter] = new Howl({
                        urls: [item.Sound],
                        onend: checkIfWin
                    });
                    item.completed = '';
                    addCommand(item.Text);
                    vm.nouns.push(item);

                    counter += 1;
                });
                AnnyangService.start();
                UserStatsFactory.newGame('Słowa - Mowa', subcategoryId);
            });

            vm.clickOnImage = function (index) {
                sounds[index].play();
            };

            function checkIfWin() {
                var isWin = true;
                _.forEach(vm.nouns, function (noun) {
                    if (noun.completed === '') {
                        isWin = false;
                    }
                });

                if (isWin) {
                    vm.isWin = isWin;
                    UserStatsFactory.gameFinished();
                }
            }

            function addCommand(name) {
                var name1 = name;
                AnnyangService.addCommand(name, function () {
                    var noun = _.find(vm.nouns, function (item) {
                        return item['Singular'].Text === name;
                    });
                    noun.completed = 'item-completed';
                    checkIfWin();
                })
            }

            vm.goBack = function () {
                AnnyangService.abort();
                $ionicHistory.goBack();
            };

            $scope.$on('$ionicView.beforeLeave', function () {
                AnnyangService.abort();
            });

        }]);
})();
