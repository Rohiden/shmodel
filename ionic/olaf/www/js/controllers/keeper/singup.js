(function () {
    'use strict';

    angular.module('olafApp').controller('SingupCtrl', [
        '$stateParams', '$ionicHistory', '$state', 'UserFactory', 'CategoryFactory', 'UserStatsFactory', '$timeout', '$ionicActionSheet', 'KeeperFactory',
        'SettingFactory', '$scope', '$http',
        function ($stateParams, $ionicHistory, $state, UserFactory, CategoryFactory, UserStatsFactory, $timeout, $ionicActionSheet, KeeperFactory, SettingFactory, $scope, $http) {
            var vm = this;
            vm.loading = false;
            vm.addNewKeeper = function () {
                if (vm.userPasswordTemp !== vm.userPassword) {
                    alert('Hasła są różne');
                    return;
                }
                else if (!validateEmail(vm.userEmail)) {
                    alert("Email jest nieprawidłowy");
                    return;
                }
                var keeperModel = {
                    Login: vm.userLogin,
                    Password: vm.userPassword,
                    Email: vm.userEmail,
                };
                vm.loading = true;

                KeeperFactory.addNewKeeper(keeperModel).then(function (response) {
                    vm.message = "";
                    vm.loading = false;

                    if (response.data != "") {
                        vm.message = response.data;
                        return;
                    }
                    vm.saving = false;
                    $state.go('start');
                });
            }

            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }

            function showAlert(isSuccess) {
                var title = isSuccess ? 'Zapisano' : 'Wystąpił error';
                var alertPopup = $ionicPopup.alert({
                    title: title
                });
                $timeout(function () {
                    alertPopup.close();
                    $state.go('start');
                }, 1000);
            }

        }]);
})();
