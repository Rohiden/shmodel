(function () {
    'use strict';

    angular.module('olafApp').controller('ManageCtrl', [
        '$stateParams', '$ionicHistory', '$state', 'UserFactory', 'CategoryFactory', 'UserStatsFactory', '$timeout', '$ionicActionSheet', 'KeeperFactory',
        'SettingFactory', '$scope', '$http', '$ionicPopup',
        function ($stateParams, $ionicHistory, $state, UserFactory, CategoryFactory, UserStatsFactory, $timeout, $ionicActionSheet, KeeperFactory, SettingFactory, $scope, $http, $ionicPopup) {
            var vm = this;
            vm.pass = '';

            var myPopup = $ionicPopup.show({
                template: '<input type="password" ng-model="vm.pass">',
                title: 'Potwierdz swoje hasło',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' },
                    {
                        text: '<b>Potwierdz</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            if (!vm.pass) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                KeeperFactory.loginUser(vm.pass).then(function (isLogged) {
                                    if (!isLogged.data) {
                                        alert("Niepoprawne hasło");
                                        e.preventDefault();
                                        return
                                    }
                                    myPopup.close();
                                    getKeppers();
                                    return vm.pass;
                                })
                                e.preventDefault();
                            }
                        }
                    },
                ]
            });

            vm.activeChange = function (item) {
                KeeperFactory.changeActivate(item.Id).then(function () {
                });
            }

            vm.removeKeeper = function (keeper) {
                KeeperFactory.removeKeeper(keeper.Id).then(function () {
                    getKeppers();
                })
            }

            vm.goBack = function () {
                $ionicHistory.goBack();
            }

            function getKeppers() {
                KeeperFactory.getAllKeppers().then(function (response) {
                    vm.keepers = response.data;
                });
            }
        }]);
})();
