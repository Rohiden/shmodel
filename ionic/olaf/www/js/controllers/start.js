(function () {
    'use strict';

    angular.module('olafApp').controller('StartCtrl', [
        '$stateParams', '$ionicHistory', '$state', 'UserFactory', 'CategoryFactory', 'UserStatsFactory', '$timeout', '$ionicActionSheet',
        'SettingFactory', 'KeeperFactory', '$scope', '$http',
        function ($stateParams, $ionicHistory, $state, UserFactory, CategoryFactory, UserStatsFactory, $timeout, $ionicActionSheet, SettingFactory, KeeperFactory, $scope, $http) {
            var vm = this;

            vm.loginUser = function () {
                vm.message = '';
                var base = btoa(vm.userLogin + ':' + vm.userPassword);
                $http.defaults.headers.common.Authorization = 'Basic ' + base;
                KeeperFactory.loginUser(vm.pass).then(function (isLogged) {
                    if (isLogged) {
                        $state.go('home');
                    }else{
                        vm.message = "Błędny login, hasło lub konto jest nieaktywne";
                    }
                });
            }

            vm.goToSingup = function () {
                $state.go('singup');
            }

        }]);
})();
