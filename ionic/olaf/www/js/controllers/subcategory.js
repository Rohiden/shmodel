(function () {
    'use strict';

    angular.module('olafApp').controller('SubcategoryCtrl', [
        'olafApi', '$stateParams', '$ionicHistory', '$state', 'SubcategoryFactory', 'ActionFactory','SettingFactory',
        function (olafApi, $stateParams, $ionicHistory, $state, SubcategoryFactory, ActionFactory,SettingFactory) {

             var  baseURL = SettingFactory.getHostURL()+'/multimedia/';
            var vm = this;
            var categoryId = Number($stateParams.id);
            vm.Subcategories = [];
            olafApi.getSubcategories(categoryId).then(function (response) {
                _.forEach(response.data, function(item){
                    item.Variance.Sound.Path = baseURL + item.Variance.Sound.Path;
                    item.Picture.Path = baseURL + item.Picture.Path;
                });
                vm.Subcategories = response.data;

                setupSoundModule(response.data);
            });


            vm.selectedsubCategory = function (index) {
                var subcategory = vm.Subcategories[index];
                var sound = sounds[subcategory.Id];
                sound.play();
            };

            vm.goBack = function () {
                $ionicHistory.goBack();
            };

            function setupSoundModule(subcategories) {
                _.forEach(subcategories, function (subcategory) {
                    sounds[subcategory.Id] = new Howl({
                        urls: [subcategory.Variance.Sound.Path],
                        onend: function () {
                            SubcategoryFactory.setValue(subcategory);
                            ActionFactory.getValue(subcategory.Id);
                            $state.go('actionType', {
                                categoryId: categoryId,
                                subcategoryId: subcategory.Id
                            });
                        }
                    });
                });
            }

            var sounds = {};
        }]);
})();
