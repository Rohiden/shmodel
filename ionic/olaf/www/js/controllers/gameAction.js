(function () {
    'use strict';

    angular.module('olafApp').controller('gameActionCtrl', [
        'olafApi', '$stateParams', '$ionicHistory', '$state','SettingFactory',
        function (olafApi, $stateParams, $ionicHistory, $state,SettingFactory) {
            var vm = this;

            var categoryId = Number($stateParams.categoryId);
            var subcategoryId = Number($stateParams.subcategoryId);
            var actionType = Number($stateParams.actionType);

            vm.baseURL = SettingFactory.getHostURL();

            vm.selectedAction = function (type) {
                var game = getGameType()[type];
                $state.go(game, {
                    actionType: type,
                    categoryId: categoryId,
                    subcategoryId: subcategoryId
                });
            };

            function getGameType() {
                var wordGames = ['wordLearn', 'wordExercise', 'wordSpeach', 'wordMemory'];
                var expressionsGames = ['expressionLearn', 'expressionExercise', 'expressionSpeach', 'expressionMemory'];
                if (actionType === 0) {
                    return wordGames;
                } else if (actionType === 1) {
                    return expressionsGames;
                }
            }

            vm.goBack = function () {
                $ionicHistory.goBack();
            };

        }]);
})();