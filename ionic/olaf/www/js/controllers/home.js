(function () {
    'use strict';

    angular.module('olafApp').controller('HomeCtrl', [
        '$stateParams', '$ionicHistory', '$state', 'UserFactory', 'CategoryFactory', 'UserStatsFactory', '$timeout', '$ionicActionSheet',
        'SettingFactory', '$scope',
        function ($stateParams, $ionicHistory, $state, UserFactory, CategoryFactory, UserStatsFactory, $timeout, $ionicActionSheet, SettingFactory, $scope) {
            var vm = this;
            var allUsers = [];
            vm.searchValue = '';
            vm.userSelected = '';
            vm.isLiteral = false;
            vm.personType = '1 os';

            vm.goToCategory = function () {
                if (vm.userSelected == '') {
                    alert('wybierz użytkownika')
                    return;
                }
                SettingFactory.setLiteral(vm.isLiteral);
                SettingFactory.setPersonType(vm.personType);

                $state.go('category');
            }

            UserFactory.getUsers().then(function (response) {
                vm.users = response;
                allUsers = response;
            });

            vm.searchUser = function () {
                var searchValue = vm.searchValue.toLowerCase().trim();
                if (searchValue === '') {
                    vm.users = allUsers;
                }
                vm.users = _.filter(allUsers, function (user) {
                    return user.Login.toLowerCase().indexOf(searchValue) !== -1 || (user.NameFirst + user.NameLast).toLowerCase().indexOf(searchValue) !== -1;
                });
            }

            vm.userChanged = function () {
                UserStatsFactory.setup(vm.userSelected);
            };

            $scope.$on('$ionicView.beforeEnter', function () {
                refreshUsers();
            });
            vm.showDetails = function (userId) {
                UserStatsFactory.setup(userId);

                // Show the action sheet
                var hideSheet = $ionicActionSheet.show({
                    buttons: [
                        {
                            text: '<b>Zobacz</b> postęp'
                        },
                        {
                            text: 'Edytuj dane'
                        }
                    ],
                    destructiveText: 'Usuń',
                    cancelText: 'Anuluj',
                    cancel: function () { },
                    buttonClicked: function (index) {
                        if (index === 0) {
                            goToUserProgress(userId);
                        } else if (index === 1) {
                            goToEdit(userId);
                        }
                    },
                    destructiveButtonClicked: function () {
                        removeUser(userId);
                    }
                });
                $timeout(function () {
                    hideSheet();
                }, 2000);

            };

            vm.addNewUser = function () {
                $state.go('newUser');
            }

            function goToUserProgress(userId) {
                $state.go('userProgress', {
                    userId: userId
                });
            }

            function goToEdit(userId) {
                $state.go('newUser', {
                    userId: userId
                });
            }

            function removeUser(userId) {
                UserFactory.removeUser(userId).then(function (response) {
                    refreshUsers();
                });
            }

            CategoryFactory.getCategories();

            function refreshUsers() {
                UserFactory.getUsers(true).then(function (response) {
                    vm.users = response;
                    allUsers = response;
                });
            }

            vm.goToManage = function () {
                $state.go('manage');
            }
            
            vm.goToHome = function () {
                $state.go('start');
            }

        }]);
})();