(function () {
    'use strict';

    angular.module('olafApp').controller('CategoryCtrl', [
        '$window', '$state', 'CategoryFactory','$ionicHistory',
        function ($window, $state, CategoryFactory,$ionicHistory) {
            var vm = this;
            var itemClicked = false;
            var maxItemInRow = 3;
            CategoryFactory.getCategories().then(function (response) {
                vm.categories = response;
                var categoriesInRows = [];
                var categoriesInRow = [];
                _.forEach(response, function (category) {
                    categoriesInRow.push(category);
                    if (categoriesInRow.length === maxItemInRow) {
                        categoriesInRows.push(categoriesInRow);
                        categoriesInRow = [];
                    }
                    sounds[category.Id] = new Howl({
                        urls: [category.Name.Sound.Path],
                        onend: function () {
                            itemClicked = false; 1
                            $state.go('subcategory', {
                                id: category.Id
                            });

                        }
                    });
                });
                categoriesInRows.push(categoriesInRow);
                vm.categoiesInRows = categoriesInRows;
                var maxHeight = $window.innerHeight / categoriesInRows.length;
                vm.maxHeightStyle = '{height: "' + maxHeight + 'px"}';
            });

            var sounds = {};

            vm.selectedCategory = function (category) {
                if (itemClicked === true) {
                    return;
                }
                itemClicked = true;
                sounds[category.Id].play();
            };

            vm.goBack = function () {
                $ionicHistory.goBack();
            };

        }]);
})();