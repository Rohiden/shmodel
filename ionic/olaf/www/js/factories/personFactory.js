angular.module('olafApp').factory('PersonFactory', [
    'olafApi', '$q', 'SettingFactory',
    function (olafApi, $q, SettingFactory) {
        var person;
        var factorySubcategoyId;
        var baseURL = olafApi.getHostURL();

        function getPerson(subcategoyId) {
            var deferred = $q.defer();
            if (person === undefined || factorySubcategoyId !== subcategoyId) {
                var personType = SettingFactory.getPersonType();
                if (personType === "1 os") {
                    var person = [];
                    person[0] = {};
                    person[0].Picture =  baseURL + "/multimedia/picture/person/ja.jpg";
                    person[0].Sound = baseURL + '/multimedia/Audio/person/ja.mp3';
                    person[0].Text = 'Ja';
                    factorySubcategoyId = subcategoyId;
                    deferred.resolve(person);
                }
                else if(personType === "2 os") {
                    olafApi.getPersonForSubcategory(subcategoyId).then(function (response) {
                        person = response.data;
                        person[0].Picture = baseURL + '/multimedia/picture/person/ty.jpg'  ;
                        person[0].Sound = baseURL + 'multimedia/Audio/person/ty.mp3';
                        person[0].Text = 'Ty';
                        factorySubcategoyId = subcategoyId;
                        deferred.resolve(person);
                    });
                }
                else {
                    olafApi.getPersonForSubcategory(subcategoyId).then(function (response) {
                        person = response.data;
                        person[0].Picture = baseURL + '/multimedia/' + person[0].Picture.Path;
                        person[0].Sound = baseURL + '/multimedia/' + person[0].Name.Sound.Path;
                        person[0].Text = person[0].Name.Text;
                        factorySubcategoyId = subcategoyId;
                        deferred.resolve(person);
                    });
                }
            } else {
                deferred.resolve(person);
            }
            return deferred.promise;
        }



        return {
            getPerson: getPerson
        };
}]);
