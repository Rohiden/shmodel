angular.module('olafApp').factory('KeeperFactory', [
    'olafApi', '$q', 'SettingFactory',
    function (olafApi, $q, SettingFactory) {
        var baseURL = olafApi.getHostURL() + '//';

        function loginUser(keeperData) {
            var deffered = $q.defer();
            olafApi.loginUser(keeperData).then(function (resp) {
                deffered.resolve(resp);
            })
            return deffered.promise;
        }

        function addNewKeeper(keeper) {
            var deferred = $q.defer();
            olafApi.addKeeper(keeper).then(function (response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        }


        function removeKeeper(keeperId) {
            var deferred = $q.defer();
            olafApi.removeKeeper(keeperId).then(function (response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        }
        function changeActivate(id) {
            var deferred = $q.defer();
            olafApi.changeActivate(id).then(function (response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        function getAllKeppers() {
            var deferred = $q.defer();
            olafApi.getAllKeppers().then(function (response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        return {
            loginUser: loginUser,
            addNewKeeper: addNewKeeper,
            getAllKeppers: getAllKeppers,
            changeActivate: changeActivate,
            removeKeeper: removeKeeper
        };
    }]);
