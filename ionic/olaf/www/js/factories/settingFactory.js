angular.module('olafApp').factory('SettingFactory', [
 '$state', 'olafApi',
    function ($state, olafApi) {
        var isLiteral;
        var personType;
        var _gameType;
        var winSong;

        function getHostURL() {
            return olafApi.getHostURL();
        }

        function checkIsLiteral() {
            if (isLiteral === undefined) {
                isLiteral = localStorage.getItem('isLiteral');
                if (isLiteral === undefined) {
                    $state.go('home');
                }
            } else {
                return isLiteral;
            }
        }

        function setLiteral(isLiteralValue) {
            isLiteral = isLiteralValue;
            localStorage.setItem('isLiteral', isLiteralValue);

        }

        function setPersonType(personTypeValue) {
            personType = personTypeValue
            localStorage.setItem('personType', personTypeValue);
        }

        function getPersonType() {
            if (personType === undefined) {
                personType = localStorage.getItem('personType');
                if (personType === undefined) {
                    $state.go('home');
                }

            } else {
                return personType;
            }
        }

        function getGameType() {
            if (_gameType === undefined) {
                _gameType = localStorage.getItem('gameType');
                if (_gameType === undefined) {
                    $state.go('home');
                }
            } else {
                return _gameType;
            }
        }

        function setGameType(gameType) {
            _gameType = gameType;
            localStorage.setItem('gameType', gameType);
        }

        function playWinSong() {
            if (winSong == undefined) {
                setupWinSont();
            }
            winSong.play();

        }

        function setupWinSont() {
            winSong = new Howl({
                urls: [getHostURL() + 'multimedia/win.mp3'],
            }).play();
        }

        return {
            checkIsLiteral: checkIsLiteral,
            setLiteral: setLiteral,
            setPersonType: setPersonType,
            getPersonType: getPersonType,
            getHostURL: getHostURL,
            setGameType: setGameType,
            getGameType: getGameType,
            playWinSong: playWinSong
        };
    }]);