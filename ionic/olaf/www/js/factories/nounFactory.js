angular.module('olafApp').factory('NounFactory', [
    'olafApi', '$q', 'SettingFactory',
    function (olafApi, $q, SettingFactory) {
        var nouns;
        var factorySubcategoyId;
        var baseURL = olafApi.getHostURL()+'/multimedia/';


        function getNouns(subcategoyId) {
            var deferred = $q.defer();
                olafApi.getNounsForSubcategory(subcategoyId).then(function (response) {
                    nouns = response.data;
                    var personType = SettingFactory.getPersonType();
                    var gameType = SettingFactory.getGameType();
                    factorySubcategoyId = subcategoyId;
                    _.forEach(nouns, function (noun) {
                        if (gameType === 0) {
                            noun.Picture = baseURL + noun.Picture.Path;
                            noun.Sound = baseURL + noun.Singular.Sound.Path;
                            noun.Text = noun.Singular.Text;
                        } else {
                            noun.Picture = baseURL + noun.Picture.Path;
                            noun.Sound = baseURL + noun.Varierty.Sound.Path;
                            noun.Text = noun.Varierty.Text;
                        }

                    })
                    deferred.resolve(nouns);
                });

            return deferred.promise;
        }

        return {
            getNouns: getNouns
        };
    }]);
