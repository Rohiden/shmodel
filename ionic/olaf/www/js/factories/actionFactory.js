angular.module('olafApp').factory('ActionFactory', [
    'olafApi', '$q', 'SettingFactory',
    function (olafApi, $q, SettingFactory) {
        var action;
        var factoryCategoryId;
        var counter = 0;
        var baseURL = olafApi.getHostURL();

        function getAction(categoryId) {
            var deferred = $q.defer();
            if (action === undefined || action === null || factoryCategoryId !== categoryId) {
                var personType = SettingFactory.getPersonType();
                olafApi.getActivity(categoryId).then(function (response) {
                    action = response.data;

                    if (personType == "1 os") {
                        action.Sound = baseURL + '/multimedia/' + action.Activity.FirstPerson.Sound.Path;
                        action.Picture = baseURL + '/multimedia/' + action.Picture.Path;
                        action.Text = action.Activity.FirstPerson.Text;
                    }
                    else if (personType == "2 os") {
                        action.Sound = baseURL + '/multimedia/' + action.Activity.SecondPerson.Sound.Path;
                        action.Picture = baseURL + '/multimedia/' + action.Picture.Path;
                        action.Text = action.Activity.SecondPerson.Text;
                    }
                    else {
                        action.Sound = baseURL + '/multimedia/' + action.Activity.ThierdPerson.Sound.Path;
                        action.Picture = baseURL + '/multimedia/' + action.Picture.Path;
                        action.Text = action.Activity.ThierdPerson.Text;
                    }
//                    action.Activity.FirstPerson.Sound.Path = baseURL + '/multimedia/' + action.Activity.FirstPerson.Sound.Path;
//                    action.Picture.Path = baseURL + '/multimedia/' + action.Picture.Path;
                    factoryCategoryId = categoryId;

                    deferred.resolve(action);
                });
                counter += 1;
            } else {
                deferred.resolve(action);
            }
            return deferred.promise;

        }

        return {
            getValue: getAction
        };
    }]);
