angular.module('olafApp').factory('UserFactory', [
    'olafApi', '$q',
    function (olafApi, $q) {
        var users;
        var userDetailsId;
        var userDetails;

        function getUserDetails(userId) {
            var deferred = $q.defer();
            if (userDetails === undefined || userDetailsId !== userId) {
                olafApi.getUserDetails(userId).then(function (response) {
                    userDetails = response.data;
                    userDetailsId = userId;
                    deferred.resolve(users);
                });
            } else {
                deferred.resolve(users);
            }
            return deferred.promise;
        }

        function getUser(userId) {
            var deferred = $q.defer();
            olafApi.getUser(userId).then(function (response) {
                deferred.resolve(response.data);
            });
            return deferred.promise;
        }

        function getUsers(ifForse) {
            var deferred = $q.defer();
            olafApi.getUsers().then(function (response) {
                users = response;
                deferred.resolve(users);
            });

            return deferred.promise;
        }

        function addUser(user) {
            var deferred = $q.defer();
            olafApi.addUser(user).then(function (response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        function removeUser(userId) {
            var deferred = $q.defer();
            olafApi.removeUser(userId).then(function (response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        }
        return {
            getUser: getUser,
            getUsers: getUsers,
            getUserDetails: getUserDetails,
            addUser: addUser,
            removeUser: removeUser
        };
    }]);