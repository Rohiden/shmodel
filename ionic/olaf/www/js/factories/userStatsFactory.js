angular.module('olafApp').factory('UserStatsFactory', [
    'olafApi', '$q', 'SettingFactory',
    function (olafApi, $q, SettingFactory) {
        var userId;
        var userStat;
        var allUserStats;

        function getUserStats(newUserId) {
            userId = newUserId;
            var deferred = $q.defer();
            if (allUserStats === undefined) {
                olafApi.getUserStats(userId).then(function (response) {
                    allUserStats = response.data;
                    deferred.resolve(allUserStats);
                });
            } else {
                deferred.resolve(allUserStats);
            }
            return deferred.promise;
        }


        function setup(newUserId) {
            localStorage.setItem('userId', newUserId);
            userId = newUserId;
        }

        function newGame(gameName, subcategoryId) {
            if (userId == undefined) {
                userId = localStorage.getItem('userId');
            }

            userStat = {
                GameName: gameName,
                IsSuccess: false,
                UserId: userId,
                SubcategoryId: subcategoryId
            }
            addUserStat();
        }

        function gameFinished() {
            SettingFactory.playWinSong();
            updateUserStat(true);
        }

        function addUserStat() {
            olafApi.addUserStat(userStat).then(function (response) {
                userStat.Id = response.data;
            });
        }

        function updateUserStat(isSuccess) {
            olafApi.updateUserStat(userStat.Id, isSuccess);
        }

        return {
            setup: setup,
            newGame: newGame,
            gameFinished: gameFinished,
            getUserStats: getUserStats
        };
    }]);