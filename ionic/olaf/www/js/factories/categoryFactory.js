angular.module('olafApp').factory('CategoryFactory', [
    'olafApi', '$q',
    function (olafApi, $q) {
        var categories;

        function getCategories() {
            var deferred = $q.defer();
            if (categories === undefined) {
                olafApi.getCategories().then(function (response) {
                    categories = response.data;
                    deferred.resolve(categories);
                });
            } else {
                deferred.resolve(categories);
            }
            return deferred.promise;
        }

        return {
            getCategories: getCategories
        };
    }]);