angular.module('olafApp').factory('SubcategoryFactory', function ($rootScope) {
    var subcategory = {};

    return {
        getValue: function () {
            return subcategory;
        },
        setValue: function (value) {
            subcategory = value;
        }
    }
});