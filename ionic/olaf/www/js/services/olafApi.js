(function () {
    'use strict';
    angular.module('olafApp').factory('olafApi', [
        '$http', '$q', 'Base64', 'HeaderProvider',
        function ($http, $q, Base64, HeaderProvider) {
            var self = this;
            // var olafAPIWeb = 'webTest/server/api/olaf';
            var olafAPIWeb = 'http://localhost:26195/api/olaf/';

            // var olafAPIWeb = 'http://localhost:26195/api/olaf';
            //  var olafAPIWeb = 'http://shmodel.pl/server/api/olaf/';
            // var olafAPIWeb = 'server/api/olaf/';

            function getHostURL() {
                return 'http://shmodel.pl/server/files/';
            }


            //TODO: comment this on web
            // $http.defaults.headers.common.Authorization = 'Basic YWRtaW46YWRtaW4=';


            function getCategories() {
                var deferred = $q.defer();
                $http.get(olafAPIWeb + '/getCategories').then(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            }

            function getSubcategories(categoryId) {
                var deferred = $q.defer();
                $http.get(olafAPIWeb + '/GetSubcategories/' + categoryId).then(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            }

            function getNounsForSubcategory(subcategoyId) {
                var deferred = $q.defer();
                $http.get(olafAPIWeb + '/getNouns?subcategoryId=' + subcategoyId).then(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            }

            function getPersonForSubcategory(subcategoryId) {
                var deferred = $q.defer();
                $http.get(olafAPIWeb + '/getPerson?subcategoryId=' + subcategoryId).then(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            }

            function getActivity(subcategoryId) {
                var deferred = $q.defer();
                $http.get(olafAPIWeb + '/getAction?subcategoryId=' + subcategoryId).then(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            }

            function getUsers() {
                // var deferred = $q.defer();
                // $http.get(olafAPIWeb + '/getUsers').then(function (response) {
                //   deferred.resolve(response);
                // });
                // return deferred.promise;


                var deferred = $q.defer();
                $http({
                    url: olafAPIWeb + '/getUsers',
                    method: 'GET',
                    header: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic YWRtaW46YWRtaW4='
                    }
                }).success(function (data) {
                    deferred.resolve(data);
                })
                return deferred.promise;

            }

            function getUser(userId) {
                // var deferred = $q.defer();
                // $http.get(olafAPIWeb + '/GetUser?userId=' + userId).then(function (response) {
                //   deferred.resolve(response);
                // });
                // return deferred.promise;
                var deferred = $q.defer();
                debugger;
                $http({
                    url: olafAPIWeb + '/GetUserassas111d?userId=' + userId,
                    method: 'GET',
                    header: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic YWRtaW46YWRtaW4='
                    }
                }).success(function (data) {
                    deferred.resolve(response);
                }).error(function (error) {
                    deferred.resolve(response);
                })
                return deferred.promise;
            }

            function addUser(user) {
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: olafAPIWeb + '/addUser',
                    data: user,
                    headers: {
                        'Content-type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                }).then(function (response) {
                    deferred.resolve(response);
                });

                return deferred.promise;

            }

            function addKeeper(keeper) {
                $http.defaults.withCredentials = false
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: olafAPIWeb + '/AddNewKeeper',
                    data: keeper,
                    headers: {
                        'Content-type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                }).then(function (response) {
                    $http.defaults.withCredentials = true;

                    deferred.resolve(response);
                });

                return deferred.promise;
            }

            function getAllKeppers() {
                var deferred = $q.defer();
                $http.get(olafAPIWeb + '/getAllKeppers').then(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            }

            function removeUser(userId) {
                var deferred = $q.defer();
                $http.get(olafAPIWeb + '/RemoveUser?userId=' + userId).then(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            }

            function addUserStat(userStat) {
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: olafAPIWeb + '/addUserStat',
                    data: userStat,
                    headers: {
                        'Content-type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                }).then(function (response) {
                    deferred.resolve(response);
                });

                return deferred.promise;
            }

            function getUserStats(userId) {
                var deferred = $q.defer();
                $http.get(olafAPIWeb + '/getUserStats?userId=' + userId).then(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            }

            function updateUserStat(statId, isSuccess) {
                var deferred = $q.defer();
                $http.get(olafAPIWeb + '/updateUserStat?statId=' + statId + '&&isSuccess=' + isSuccess).then(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            }

            function loginUser(keeperData) {
                var deffered = $q.defer();
                $http.get(olafAPIWeb + '/confirmPassword?pass=' + keeperData).then(function (resp) {
                    deffered.resolve(resp);
                });
                return deffered.promise;
            }

            function changeActivate(id) {
                var deffered = $q.defer();
                $http.get(olafAPIWeb + '/activate?keeperId=' + id).then(function (resp) {
                    deffered.resolve(resp);
                });
                return deffered.promise;
            }

            function removeKeeper(id) {
                var deffered = $q.defer();
                $http.get(olafAPIWeb + '/removeKeeper?keeperId=' + id).then(function (resp) {
                    deffered.resolve(resp);
                });
                return deffered.promise;
            }
            return {
                getCategories: getCategories,
                getSubcategories: getSubcategories,
                getNounsForSubcategory: getNounsForSubcategory,
                getPersonForSubcategory: getPersonForSubcategory,
                getActivity: getActivity,
                getUsers: getUsers,
                getUser: getUser,
                addUser: addUser,
                removeUser: removeUser,
                addUserStat: addUserStat,
                getUserStats: getUserStats,
                updateUserStat: updateUserStat,
                getHostURL: getHostURL,
                addKeeper: addKeeper,

                getAllKeppers: getAllKeppers,
                loginUser: loginUser,
                changeActivate: changeActivate,
                removeKeeper: removeKeeper
            };

        }
    ]);

})();
