﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlafModels.Models
{
    public class Noun
    {
        public int Id { get; set; }

        public int SingularId { get; set; }
        public virtual Variance Singular { get; set; }

        public int VariertyId { get; set; }
        public virtual Variance Varierty { get; set; }
    }
}
