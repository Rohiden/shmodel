﻿namespace OlafModels.Models
{
    public class WordType
    {
        public int Id { get; set; }

        public int? NounId { get; set; }
        public virtual Noun Noun { get; set; }

        public int? ActivityId { get; set; }
        public virtual Activity Activity { get; set; }

        public int SubcategoryId { get; set; }
        public virtual Subcategory Subcategory { get; set; }

        public int PictureId { get; set; }
        public virtual Picture Picture { get; set; }
    }
}
