﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlafModels.Models
{
    public class Person
    {
        public int Id { get; set; }

        public int NameId { get; set; }
        public virtual Variance Name { get; set; }

        public int PictureId { get; set; }
        public virtual Picture Picture { get; set; }
    }
}
