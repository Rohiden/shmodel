﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OlafModels.Models
{
    public class UserStat
    {
        public int Id { get; set; }
        public string GameName { get; set; }
        public DateTime StartGame { get; set; }
        public DateTime? EndGame { get; set; }
        public bool IsSuccess { get; set; }

        public int SubcategoryId { get; set; }
        [ForeignKey("SubcategoryId")]
        public Subcategory Subcategory { get; set; }

        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}

