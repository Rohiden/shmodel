﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlafModels.Models
{
    public class Activity
    {
        public int Id { get; set; }

        
        public int InfinitiveId { get; set; }
        [ForeignKey("InfinitiveId")]
        public virtual Variance Infinitive { get; set; }

        public int FirstPersonId { get; set; }
        [ForeignKey("FirstPersonId")]
        public virtual Variance FirstPerson { get; set; }

        public int SecondPersonId { get; set; }
        [ForeignKey("SecondPersonId")]
        public virtual Variance SecondPerson { get; set; }

        public int ThierdPersonId { get; set; }
        [ForeignKey("ThierdPersonId")]
        public virtual Variance ThierdPerson { get; set; }

    }
}
