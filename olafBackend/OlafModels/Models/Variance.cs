﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlafModels.Models
{
    public class Variance
    {
        public int Id { get; set; }
        public string Text { get; set; }


        public int SoundId { get; set; }
        public virtual Sound Sound { get; set; }
    }
}
