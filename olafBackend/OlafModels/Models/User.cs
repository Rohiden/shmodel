﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlafModels.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string NameFirst { get; set; }
        public string NameLast { get; set; }

        public DateTime? LastLogin { get; set; }
    }
}
