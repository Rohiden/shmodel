﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlafModels.Models
{
    public class Subcategory
    {
        public int Id { get; set; }

        public int VarianceId { get; set; }
        public virtual Variance Variance { get; set; }

        public List<int> PersonsId{ get; set; }
        public virtual List<Person> Persons { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public int PictureId { get; set; }
        public virtual Picture Picture { get; set; }
    }
}
