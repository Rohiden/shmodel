﻿using System.Data.Entity;
using OlafModels.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace OlafModels
{
    public class OlafContext : DbContext
    {
        public OlafContext()
            : base("OlafDatabase")
        {
            base.Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DbSet<Activity> Activities { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Subcategory> Subcategories { get; set; }        
        public DbSet<Noun> Nouns { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Sound> Sounds { get; set; }
        public DbSet<Variance> Variances { get; set; }
        public DbSet<WordType> WordTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserStat> UserStats { get; set; }
        public DbSet<Keeper> Keepers { get; set; }

    }
}
