﻿using OlafModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlafModels.Wrappers
{
    public class NounWrapper
    {
        public int Id { get; set; }
        
        public Variance Singular { get; set; }
        
        public Variance Varierty { get; set; }

        public Picture Picture { get; set; }

        public NounWrapper(WordType wordType)
        {
            Id = wordType.Id;
            Singular = wordType.Noun.Singular;
            Varierty = wordType.Noun.Varierty;
            Picture = wordType.Picture;
        }

        public NounWrapper()
        {

        }

    }
}
