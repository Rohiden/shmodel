namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeguidToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Keepers", "UserGuid", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Keepers", "UserGuid", c => c.Guid(nullable: false));
        }
    }
}
