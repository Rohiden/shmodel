namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActiveAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Keepers", "Email", c => c.String());
            AddColumn("dbo.Keepers", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Keepers", "IsAdmin", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Keepers", "IsAdmin");
            DropColumn("dbo.Keepers", "IsActive");
            DropColumn("dbo.Keepers", "Email");
        }
    }
}
