namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addGuidToKeeper : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Keepers", "UserGuid", c => c.Guid(nullable: false));
            AddColumn("dbo.Keepers", "IsConfirm", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Keepers", "IsConfirm");
            DropColumn("dbo.Keepers", "UserGuid");
        }
    }
}
