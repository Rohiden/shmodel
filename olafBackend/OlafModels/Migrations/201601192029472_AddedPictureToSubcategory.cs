namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPictureToSubcategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Subcategories", "PictureId", c => c.Int(nullable: false));
            CreateIndex("dbo.Subcategories", "PictureId");
            AddForeignKey("dbo.Subcategories", "PictureId", "dbo.Pictures", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subcategories", "PictureId", "dbo.Pictures");
            DropIndex("dbo.Subcategories", new[] { "PictureId" });
            DropColumn("dbo.Subcategories", "PictureId");
        }
    }
}
