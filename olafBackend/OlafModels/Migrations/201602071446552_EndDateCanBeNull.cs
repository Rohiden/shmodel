namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EndDateCanBeNull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserStats", "EndGame", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserStats", "EndGame", c => c.DateTime(nullable: false));
        }
    }
}
