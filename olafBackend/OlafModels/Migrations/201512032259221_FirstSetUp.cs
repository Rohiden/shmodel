namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstSetUp : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InfinitiveId = c.Int(nullable: false),
                        FirstPersonId = c.Int(nullable: false),
                        ThierdPersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Variances", t => t.FirstPersonId)
                .ForeignKey("dbo.Variances", t => t.InfinitiveId)
                .ForeignKey("dbo.Variances", t => t.ThierdPersonId)
                .Index(t => t.InfinitiveId)
                .Index(t => t.FirstPersonId)
                .Index(t => t.ThierdPersonId);
            
            CreateTable(
                "dbo.Variances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        SoundId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sounds", t => t.SoundId)
                .Index(t => t.SoundId);
            
            CreateTable(
                "dbo.Sounds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Path = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NameId = c.Int(nullable: false),
                        PictureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Variances", t => t.NameId)
                .ForeignKey("dbo.Pictures", t => t.PictureId)
                .Index(t => t.NameId)
                .Index(t => t.PictureId);
            
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Path = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Nouns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SingularId = c.Int(nullable: false),
                        VariertyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Variances", t => t.SingularId)
                .ForeignKey("dbo.Variances", t => t.VariertyId)
                .Index(t => t.SingularId)
                .Index(t => t.VariertyId);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NameId = c.Int(nullable: false),
                        PictureId = c.Int(nullable: false),
                        Subcategory_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Variances", t => t.NameId)
                .ForeignKey("dbo.Pictures", t => t.PictureId)
                .ForeignKey("dbo.Subcategories", t => t.Subcategory_Id)
                .Index(t => t.NameId)
                .Index(t => t.PictureId)
                .Index(t => t.Subcategory_Id);
            
            CreateTable(
                "dbo.Subcategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VarianceId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .ForeignKey("dbo.Variances", t => t.VarianceId)
                .Index(t => t.VarianceId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.WordTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NounId = c.Int(),
                        ActivityId = c.Int(),
                        SubcategoryId = c.Int(nullable: false),
                        PictureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activities", t => t.ActivityId)
                .ForeignKey("dbo.Nouns", t => t.NounId)
                .ForeignKey("dbo.Pictures", t => t.PictureId)
                .ForeignKey("dbo.Subcategories", t => t.SubcategoryId)
                .Index(t => t.NounId)
                .Index(t => t.ActivityId)
                .Index(t => t.SubcategoryId)
                .Index(t => t.PictureId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WordTypes", "SubcategoryId", "dbo.Subcategories");
            DropForeignKey("dbo.WordTypes", "PictureId", "dbo.Pictures");
            DropForeignKey("dbo.WordTypes", "NounId", "dbo.Nouns");
            DropForeignKey("dbo.WordTypes", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.Subcategories", "VarianceId", "dbo.Variances");
            DropForeignKey("dbo.People", "Subcategory_Id", "dbo.Subcategories");
            DropForeignKey("dbo.Subcategories", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.People", "PictureId", "dbo.Pictures");
            DropForeignKey("dbo.People", "NameId", "dbo.Variances");
            DropForeignKey("dbo.Nouns", "VariertyId", "dbo.Variances");
            DropForeignKey("dbo.Nouns", "SingularId", "dbo.Variances");
            DropForeignKey("dbo.Categories", "PictureId", "dbo.Pictures");
            DropForeignKey("dbo.Categories", "NameId", "dbo.Variances");
            DropForeignKey("dbo.Activities", "ThierdPersonId", "dbo.Variances");
            DropForeignKey("dbo.Activities", "InfinitiveId", "dbo.Variances");
            DropForeignKey("dbo.Activities", "FirstPersonId", "dbo.Variances");
            DropForeignKey("dbo.Variances", "SoundId", "dbo.Sounds");
            DropIndex("dbo.WordTypes", new[] { "PictureId" });
            DropIndex("dbo.WordTypes", new[] { "SubcategoryId" });
            DropIndex("dbo.WordTypes", new[] { "ActivityId" });
            DropIndex("dbo.WordTypes", new[] { "NounId" });
            DropIndex("dbo.Subcategories", new[] { "CategoryId" });
            DropIndex("dbo.Subcategories", new[] { "VarianceId" });
            DropIndex("dbo.People", new[] { "Subcategory_Id" });
            DropIndex("dbo.People", new[] { "PictureId" });
            DropIndex("dbo.People", new[] { "NameId" });
            DropIndex("dbo.Nouns", new[] { "VariertyId" });
            DropIndex("dbo.Nouns", new[] { "SingularId" });
            DropIndex("dbo.Categories", new[] { "PictureId" });
            DropIndex("dbo.Categories", new[] { "NameId" });
            DropIndex("dbo.Variances", new[] { "SoundId" });
            DropIndex("dbo.Activities", new[] { "ThierdPersonId" });
            DropIndex("dbo.Activities", new[] { "FirstPersonId" });
            DropIndex("dbo.Activities", new[] { "InfinitiveId" });
            DropTable("dbo.WordTypes");
            DropTable("dbo.Subcategories");
            DropTable("dbo.People");
            DropTable("dbo.Nouns");
            DropTable("dbo.Pictures");
            DropTable("dbo.Categories");
            DropTable("dbo.Sounds");
            DropTable("dbo.Variances");
            DropTable("dbo.Activities");
        }
    }
}
