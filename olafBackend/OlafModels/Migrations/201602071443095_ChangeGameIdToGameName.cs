namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeGameIdToGameName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserStats", "GameName", c => c.String());
            DropColumn("dbo.UserStats", "GameId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserStats", "GameId", c => c.Int(nullable: false));
            DropColumn("dbo.UserStats", "GameName");
        }
    }
}
