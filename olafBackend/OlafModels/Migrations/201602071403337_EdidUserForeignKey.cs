namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EdidUserForeignKey : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UserStats", new[] { "User_Id" });
            RenameColumn(table: "dbo.UserStats", name: "User_Id", newName: "UserId");
            AlterColumn("dbo.UserStats", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.UserStats", "UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserStats", new[] { "UserId" });
            AlterColumn("dbo.UserStats", "UserId", c => c.Int());
            RenameColumn(table: "dbo.UserStats", name: "UserId", newName: "User_Id");
            CreateIndex("dbo.UserStats", "User_Id");
        }
    }
}
