namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUserStat : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        NameFirst = c.String(),
                        NameLast = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserStats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GameId = c.Int(nullable: false),
                        StartGame = c.DateTime(nullable: false),
                        EndGame = c.DateTime(nullable: false),
                        IsSuccess = c.Boolean(nullable: false),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            AddColumn("dbo.Subcategories", "UserStat_Id", c => c.Int());
            CreateIndex("dbo.Subcategories", "UserStat_Id");
            AddForeignKey("dbo.Subcategories", "UserStat_Id", "dbo.UserStats", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserStats", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Subcategories", "UserStat_Id", "dbo.UserStats");
            DropIndex("dbo.UserStats", new[] { "User_Id" });
            DropIndex("dbo.Subcategories", new[] { "UserStat_Id" });
            DropColumn("dbo.Subcategories", "UserStat_Id");
            DropTable("dbo.UserStats");
            DropTable("dbo.Users");
        }
    }
}
