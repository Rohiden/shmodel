namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovePictureToSubcategory : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Subcategories", "PictureId", "dbo.Pictures");
            DropIndex("dbo.Subcategories", new[] { "PictureId" });
            DropColumn("dbo.Subcategories", "PictureId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Subcategories", "PictureId", c => c.Int(nullable: false));
            CreateIndex("dbo.Subcategories", "PictureId");
            AddForeignKey("dbo.Subcategories", "PictureId", "dbo.Pictures", "Id");
        }
    }
}
