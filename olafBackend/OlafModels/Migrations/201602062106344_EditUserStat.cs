namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditUserStat : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Subcategories", "UserStat_Id", "dbo.UserStats");
            DropIndex("dbo.Subcategories", new[] { "UserStat_Id" });
            AddColumn("dbo.UserStats", "SubcategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.UserStats", "SubcategoryId");
            AddForeignKey("dbo.UserStats", "SubcategoryId", "dbo.Subcategories", "Id");
            DropColumn("dbo.Subcategories", "UserStat_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Subcategories", "UserStat_Id", c => c.Int());
            DropForeignKey("dbo.UserStats", "SubcategoryId", "dbo.Subcategories");
            DropIndex("dbo.UserStats", new[] { "SubcategoryId" });
            DropColumn("dbo.UserStats", "SubcategoryId");
            CreateIndex("dbo.Subcategories", "UserStat_Id");
            AddForeignKey("dbo.Subcategories", "UserStat_Id", "dbo.UserStats", "Id");
        }
    }
}
