namespace OlafModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class secondPerson : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "SecondPersonId", c => c.Int(nullable: false));
            CreateIndex("dbo.Activities", "SecondPersonId");
            AddForeignKey("dbo.Activities", "SecondPersonId", "dbo.Variances", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Activities", "SecondPersonId", "dbo.Variances");
            DropIndex("dbo.Activities", new[] { "SecondPersonId" });
            DropColumn("dbo.Activities", "SecondPersonId");
        }
    }
}
