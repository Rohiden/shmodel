﻿using OlafModels;
using OlafModels.Models;
using OlafModels.Wrappers;
using OlafRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlafRepository.Repositories
{
    public class OlafEFRepository : IOlafRepository
    {
        public IList<Category> GetCategories()
        {
            using (var context = new OlafContext())
            {
                return context.Categories.Include("Picture").Include("Name").Include("Name.Sound").ToList();
            }
        }

        public IList<Subcategory> GetSubcategories(int id)
        {
            using (var context = new OlafContext())
            {
                return context.Subcategories.Include("Variance").Include("Variance.Sound").Include("Picture").Where(x => x.CategoryId == id).ToList();
            }
        }

        public IList<NounWrapper> GetNouns(int subcategoryId)
        {
            var nouns = new List<NounWrapper>();
            using (var context = new OlafContext())
            {
                if (subcategoryId == 0)
                {
                    var fake = context.WordTypes.Include("Noun").
    Include("Picture").Include("Noun.Singular").Include("Noun.Singular.Sound").Include("Noun.Varierty").Include("Noun.Varierty.Sound")
    .Where(x => x.Noun != null).ToList();
                    var rand = new Random();
                    var random = rand.Next(fake.Min(x => x.SubcategoryId), fake.Max(x => x.SubcategoryId));
                    var temp = fake.Where(x => x.SubcategoryId == random);
                    foreach (var item in temp)
                    {
                        nouns.Add(new NounWrapper(item));
                    }
                    return nouns;
                }
                var nounsId = context.WordTypes.Include("Noun").
                    Include("Picture").Include("Noun.Singular").Include("Noun.Singular.Sound").Include("Noun.Varierty").Include("Noun.Varierty.Sound")
                    .Where(x => x.SubcategoryId == subcategoryId && x.Noun != null).ToList();
                foreach (var item in nounsId)
                {
                    nouns.Add(new NounWrapper(item));
                }
                return nouns;
            }
        }

        public static bool LoginKeeper(string userName, string password)
        {
            using (var context = new OlafContext())
            {
                var user = context.Keepers.FirstOrDefault(x => x.Login == userName);
                return user != null && EncryptionHelper.Decrypt(user.Password) == password && user.IsActive;
            }
        }

        public bool RemoveUser(int userId)
        {
            using (var context = new OlafContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Id == userId);
                var userStat = context.UserStats.Where(x => x.UserId == userId);
                context.UserStats.RemoveRange(userStat);
                context.Users.Remove(user);
                context.SaveChanges();
                return true;
            }
        }

        public User GetUser(int userId)
        {
            using (var context = new OlafContext())
            {
                return context.Users.FirstOrDefault(x => x.Id == userId);
            }
        }

        public int AddUserStat(UserStat userStat)
        {
            using (var context = new OlafContext())
            {
                userStat.StartGame = DateTime.Now;
                context.UserStats.Add(userStat);
                context.SaveChanges();
                return userStat.Id;
            }
        }

        public int UpdateUserStat(int statId, bool isSuccess)
        {
            using (var context = new OlafContext())
            {
                var stat = context.UserStats.FirstOrDefault(x => x.Id == statId);
                stat.EndGame = DateTime.Now;
                stat.IsSuccess = isSuccess;
                context.SaveChanges();
                return 1;
            }
        }

        public IList<UserStat> GetUserStats(int userId)
        {
            using (var context = new OlafContext())
            {
                return context.UserStats.Include("Subcategory").Include("Subcategory.Variance").Where(x => x.UserId == userId).OrderByDescending(x => x.StartGame).ToList();
            }
        }

        public IList<User> GetUsers()
        {
            using (var context = new OlafContext())
            {
                return context.Users.OrderByDescending(x => x.LastLogin).ToList();
            }
        }

        public int AddUser(User user)
        {
            using (var context = new OlafContext())
            {
                var dbUser = context.Users.FirstOrDefault(x => x.Id == user.Id);
                if (dbUser == null)
                {
                    context.Users.Add(user);
                }
                else
                {
                    dbUser.Login = user.Login;
                    dbUser.NameFirst = user.NameFirst;
                    dbUser.NameLast = user.NameLast;
                }
                context.SaveChanges();
                return user.Id;
            }
        }

        public WordType GetAction(int subcategoryId)
        {
            using (var context = new OlafContext())
            {
                return context.WordTypes.Include("Activity")
                     .Include("Activity.Infinitive").Include("Activity.Infinitive.Sound")
                     .Include("Activity.FirstPerson").Include("Activity.FirstPerson.Sound")
                     .Include("Activity.SecondPerson").Include("Activity.SecondPerson.Sound")
                     .Include("Activity.ThierdPerson").Include("Activity.ThierdPerson.Sound")
                     .Include("Picture")
                     .FirstOrDefault(x => x.SubcategoryId == subcategoryId);
            }
        }

        public IList<Person> GetPerson(int subcategoryId)
        {
            using (var context = new OlafContext())
            {
                return context.Subcategories.Include("Persons").Include("Persons.Name").Include("Persons.Name.Sound").Include("Persons.Picture").FirstOrDefault(x => x.Id == subcategoryId).Persons.ToList();
            }
        }

        public string AddNewKeeper(Keeper keeper)
        {
            keeper.IsConfirm = false;
            keeper.Password = EncryptionHelper.Encrypt(keeper.Password);
            keeper.UserGuid = Guid.NewGuid().ToString();
            using (var context = new OlafContext())
            {
                var keeperTemp = context.Keepers.FirstOrDefault(x => x.Login == keeper.Login);
                if (keeperTemp != null)
                {
                    return String.Empty;
                }

                context.Keepers.Add(keeper);
                context.SaveChanges();
                return keeper.UserGuid.ToString();
            }
        }

        public void ConfirmKeeper(string userGuid)
        {
            using (var context = new OlafContext())
            {
                var keeper = context.Keepers.FirstOrDefault(x => x.UserGuid.ToLower() == userGuid);
                if (keeper != null)
                {
                    keeper.IsConfirm = true;
                    keeper.IsActive = true;
                }
                context.SaveChanges();
            }
        }

        public List<Keeper> GetAllKeppers()
        {
            using (var context = new OlafContext())
            {
                return context.Keepers.Where(x=>x.Login != "shAdmin" ).ToList();
            }

        }

        public void ActivateKeeper(int keeperId)
        {
            using (var context = new OlafContext())
            {
                var keeper = context.Keepers.FirstOrDefault(x => x.Id == keeperId);
                keeper.IsActive = !keeper.IsActive;
                context.SaveChanges();
            }
        }

        public void RemoveKeeper(int keeperId)
        {
            using (var context = new OlafContext())
            {
                var keeper = context.Keepers.FirstOrDefault(x => x.Id == keeperId);
                context.Keepers.Remove(keeper);
                context.SaveChanges();
            }
        }
    }
}
