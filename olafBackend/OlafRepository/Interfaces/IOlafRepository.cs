﻿using OlafModels.Models;
using OlafModels.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlafRepository.Interfaces
{
    public interface IOlafRepository
    {
        IList<Category> GetCategories();
        IList<Subcategory> GetSubcategories(int id);
        IList<NounWrapper> GetNouns(int subcategoryId);
        bool RemoveUser(int userId);
        User GetUser(int userId);
        int AddUserStat(UserStat userStat);
        int UpdateUserStat(int statId, bool isSuccess);
        WordType GetAction(int subcategoryId);
        IList<Person> GetPerson(int subcategoryId);
        IList<UserStat> GetUserStats(int userId);
        string AddNewKeeper(Keeper keeper);
        IList<User> GetUsers();
        int AddUser(User user);
        void ConfirmKeeper(string userGuid);
        List<Keeper> GetAllKeppers();
        void ActivateKeeper(int keeperId);
        void RemoveKeeper(int keeperId);
    }
}
