﻿using OlafModels;
using OlafModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateDB.Moduls
{
    public class GenerateCategories
    {
        public void Generate()
        {
            var categoriesNames = new List<string>()
            {
                "Zajęcia z funkcjonowana w środowisku",
                "Ubrania",
                "Srodki transportu",
                "Zajęcia muzyczne i plastyczne",
                "Śniadanie i drugie śniadanie",
                "Obiad i deser",
                "Łazienka",
                "Pokój",
                "Kolacja",
                "Urządzenia"
            };

            var variancesId = CreateVariance(categoriesNames);
            var picturesId = CreatePicture(categoriesNames);

            var categories = new List<Category>();
            for (int i = 0; i < categoriesNames.Count; i++)
            {
                categories.Add(new Category { NameId = variancesId[i], PictureId = picturesId[i] });
            }

            using (var context = new OlafContext())
            {
                context.Categories.AddRange(categories);
                context.SaveChanges();
            }
        }

        private List<int> CreatePicture(List<string> categoriesNames)
        {
            var pictures = new List<Picture>();
            for (int i = 0; i < categoriesNames.Count; i++)
            {
                pictures.Add(new Picture() { Path = @"D:\Git\olagmgr\GenerateDB\Assets\Categories\Category" + i + ".jpg" });
            }
            using (var context = new OlafContext())
            {
                context.Pictures.AddRange(pictures);
                context.SaveChanges();
            }
            return pictures.Select(x => x.Id).ToList();
        }


        private List<int> CreateVariance(List<string> names)
        {
            var listOfVariance = new List<Variance>();
            var sounds = new List<Sound>();
            for (int i = 0; i < 10; i++)
            {
                sounds.Add(new Sound() { Path = @"D:\Git\olagmgr\GenerateDB\Assets\Categories\Category" + i + ".wav" });
            }
            var counter = 0;
            foreach (var item in names)
            {
                listOfVariance.Add(new Variance()
                {
                    Sound = sounds[counter],
                    Text = item,
                });
                counter += 1;
            }

            using (var context = new OlafContext())
            {
                context.Variances.AddRange(listOfVariance);
                context.SaveChanges();
            }
            return listOfVariance.Select(x => x.Id).ToList();
        }

    }
}
