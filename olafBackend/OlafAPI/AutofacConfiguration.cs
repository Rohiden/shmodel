﻿using Autofac;
using Autofac.Integration.WebApi;
using OlafRepository.Interfaces;
using OlafRepository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;

namespace OlafAPI
{
    public static class AutofacConfiguration
    {
        public static IContainer Create(HttpConfiguration configuration)
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<OlafEFRepository>().As<IOlafRepository>();

            var container = builder.Build(); 
            return container;
        }
    }
}

