﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OlafRepository.Repositories;
using OlafModels.Models;
using OlafModels.Wrappers;
using OlafRepository.Interfaces;
using MailService;
using System.Web.Http.Cors;
using System.Threading;

namespace OlafAPI.Controllers
{

    [EnableCors(origins: "http://localhost:8100", headers: "*", methods: "*")]
    public class OlafController : ApiController
    {
        IOlafRepository _olafRepository;
        EmailService _emailService;

        public OlafController(IOlafRepository olafRepository)
        {
            _olafRepository = olafRepository;
            _emailService = new EmailService();
        }

        [AllowAnonymous]
        [HttpPost]
        public string AddNewKeeper(Keeper keeper)
        {
            var userGuid = _olafRepository.AddNewKeeper(keeper);
            if (String.IsNullOrEmpty(userGuid))
            {
                return "Error: Taki login juz istnieje";
            }
            var resp = String.Empty;
            try
            {
                resp = _emailService.SendVerifiEmail(userGuid, keeper.Email);

            }
            catch (Exception ex)
            {
                resp = "Error wyspąpił podczas wysyłania emaila: " + ex.Message;
            }
            return resp;
        }

        [Auth]
        [HttpGet]
        public List<Keeper> GetAllKeppers(Keeper keeper)
        {
            var keppers = _olafRepository.GetAllKeppers();
            return keppers;
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage ConfirmKeeper(string userGuid)
        {
            _olafRepository.ConfirmKeeper(userGuid);
            var response = Request.CreateResponse(HttpStatusCode.Moved);
            response.Headers.Location = new Uri("http://www.shmodel.pl/#/start");
            return response;
        }

        [Auth]
        [HttpGet]
        public bool ConfirmPassword(string pass)
        {
            var userName = Thread.CurrentPrincipal.Identity.Name;
            return OlafEFRepository.LoginKeeper(userName, pass);
        }

        [Auth]
        [HttpGet]
        public List<Category> GetCategories()
        {
            return _olafRepository.GetCategories().ToList();
        }
        [Auth]
        public List<Subcategory> GetSubcategories(int id)
        {
            return _olafRepository.GetSubcategories(id).ToList();
        }
        [Auth]
        public List<NounWrapper> GetNouns(int subcategoryId)
        {
            return _olafRepository.GetNouns(subcategoryId).ToList();
        }
        [Auth]
        public List<Person> GetPerson(int subcategoryId)
        {
            return _olafRepository.GetPerson(subcategoryId).ToList();
        }
        [Auth]
        public WordType GetAction(int subcategoryId)
        {
            return _olafRepository.GetAction(subcategoryId);
        }
        [Auth]
        [HttpPost]
        public int AddUser(User user)
        {
            return _olafRepository.AddUser(user);
        }
        [Auth]
        [HttpGet]
        public void RemoveUser(int userId)
        {
            _olafRepository.RemoveUser(userId);
        }
        [Auth]
        [HttpGet]
        public User GetUser(int userId)
        {
            return _olafRepository.GetUser(userId);
        }

        [Auth]
        [HttpGet]
        public void Activate(int keeperId)
        {
            _olafRepository.ActivateKeeper(keeperId);
        }

        [Auth]
        [HttpGet]
        public void RemoveKeeper(int keeperId)
        {
            _olafRepository.RemoveKeeper(keeperId);
        }

        [Auth]
        [HttpPost]
        public int AddUserStat(UserStat userStat)
        {
            return _olafRepository.AddUserStat(userStat);
        }
        [Auth]
        [HttpGet]
        public void UpdateUserStat(int statId, bool isSuccess)
        {
            _olafRepository.UpdateUserStat(statId, isSuccess);
        }
        [Auth]
        public List<User> GetUsers()
        {
            var user = _olafRepository.GetUsers().ToList(); ;
            return _olafRepository.GetUsers().ToList();
        }
        [Auth]
        public List<UserStat> GetUserStats(int userId)
        {
            return _olafRepository.GetUserStats(userId).ToList();
        }

    }
}
