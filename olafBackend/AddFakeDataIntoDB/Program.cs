﻿using oldData = LoadOldData;
using OlafModels;
using OlafModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AddFakeDataIntoDB
{
    class Program
    {
        static void Main(string[] args)
        {

            //Console.WriteLine("Removing");
            //RemoveAllData();
            //Console.WriteLine("Removed");

            //Console.WriteLine("Adding category");
            //AddCategories();
            //Console.WriteLine("Adding subcategory");
            //AddSubcategories();
            //Console.WriteLine("Adding nouns");
            //AddNoun();

            removeFirst40WordType();


            Console.WriteLine("Generate users");
            //GenerateUsers();
        }

        private static void removeFirst40WordType()
        {
            using (var context = new OlafContext())
            {

                    context.Database.ExecuteSqlCommand("Delete Top (40) from WordTypes");
                    context.SaveChanges();                
            }
        }

        private static void GenerateUsers()
        {
            var user = new User()
            {
                LastLogin = DateTime.Now.AddDays(-2),
                Login = "amickiewicz",
                NameFirst = "Adam",
                NameLast = "Mickiewicz"
            };
            using (var context = new OlafContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }

            var userStat = new List<UserStat>()
            {
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),

                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),
                getRandomStat(user.Id),



            };

            using (var context =new OlafContext())
            {
                context.UserStats.AddRange(userStat);
                context.SaveChanges();
            }
        }

        private static UserStat getRandomStat(int userId)
        {
            var randInt = RandomNumber(1, 5);
            var startDay = DateTime.Now.AddDays(randInt).AddHours(RandomNumber(1, 5));
            var randSec = RandomNumber(60,120);
            var endDate = startDay.AddSeconds(randSec);
            var randSucces = RandomNumber(0,2);
            var GameNames = new List<string>() { "Wypowiedź - Mowa", "Wypowiedź - Ćwiczę", "Wypowiedź - Ucze się", "Wypowiedź - Pamięć", "Słowa - Ćwiczę", "Słowa - Ucze się", "Słowa - Pamięć", "Słowa - Mowa" };
            var gameId = RandomNumber(0,7);
            var userStat = new UserStat()
            {
                EndGame = endDate,
                IsSuccess = randSucces % 2 == 0,
                StartGame = startDay,
                SubcategoryId = 910,        //TODO: fix this
                GameName = GameNames[gameId],
                UserId = userId
            };
            return userStat;
        }

        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return random.Next(min, max);
            }
        }

        private static void RemoveAllData()
        {
            Console.WriteLine("Remove all data?");
            var key = Console.ReadKey();
            if (key.KeyChar != 'y')
            {
                return;
            }
            Console.WriteLine("Removing...");
            var listOfTables = new List<string>()
            {
                "WordTypes",
                "Activities",
                "Nouns",
                "People",
                "UserStats",
                "Users",
                "Subcategories",
                "Categories",
                "Pictures",
                "Variances",
                "Sounds"

            };
            using (var context = new OlafContext())
            {
                foreach (var item in listOfTables)
                {
                    context.Database.ExecuteSqlCommand("Delete from " + item);
                    context.SaveChanges();
                }
            }
        }

 

        private static void AddNoun()
        {

            var itemCounter = 0;
            var categoryCounter = 0;
            var subcategoryCounter = 0;

            var subcategoryId = 0;
            using (var context = new OlafContext())
            {
                var tempCategories = context.Subcategories.ToList();

                subcategoryId = tempCategories.First().Id;
            }

            var oldNouns = new List<oldData.Noun>();
            using (var piLearnContext = new oldData.PILearnEntities())
            {
                oldNouns.AddRange(piLearnContext.Noun.ToList());
            }

            String fromPath;
            String toPath;
            var counter = 1;
            var wordTypes = new List<WordType>();
            foreach (var oldNoun in oldNouns)
            {
                var word = new WordType();
                fromPath = String.Format(@"Multimedia\c{0}s{1}i{2}.jpg", categoryCounter, subcategoryCounter, itemCounter);
                toPath = String.Format(@"Picture\Noun\{0}.jpg", oldNoun.Singular);
                //CoppyFile(fromPath, toPath);

                word.Picture = new Picture()
                {
                    Path = StripText(toPath)
                };
                word.SubcategoryId = subcategoryId;

                fromPath = String.Format(@"Audio\Noun\c{0}s{1}i{2}_Singular.mp3", categoryCounter, subcategoryCounter, itemCounter);
                String singularPath = String.Format(@"Audio\Noun\Singular\{0}.mp3", oldNoun.Singular);
                //CoppyFile(fromPath, singularPath);

                fromPath = String.Format(@"Audio\Noun\c{0}s{1}i{2}_Variety.mp3", categoryCounter, subcategoryCounter, itemCounter);
                String VarietyPath = String.Format(@"Audio\Noun\Variety\{0}.mp3", oldNoun.Variety);
                //CoppyFile(fromPath, VarietyPath);

                word.Noun = new Noun()
                {
                    Singular = new Variance() { Sound = new Sound() { Path = StripText(singularPath) }, Text = oldNoun.Singular },
                    Varierty = new Variance() { Sound = new Sound() { Path = StripText(VarietyPath) }, Text = oldNoun.Variety }
                };

                wordTypes.Add(word);
                if (counter % 3 == 0 && counter != 120)
                {
                    subcategoryId += 1;
                }


                if (itemCounter > 1)
                {
                    itemCounter = 0;
                    subcategoryCounter += 1;
                }
                else {
                    itemCounter += 1;
                }
                if (subcategoryCounter
                    > 3)
                {
                    subcategoryCounter = 0;
                    categoryCounter += 1;
                }
                counter += 1;
            }

            using (var context = new OlafContext())
            {
                context.WordTypes.AddRange(wordTypes);
                context.SaveChanges();
            }
        }

        static void CoppyFile(string FormPath, string ToPatch)
        {
            var sourcePath = @"D:\Git\mgrOlaf\muilimedia\" + FormPath;
            var destPath = @"D:\Git\mgrOlaf\olafBackend\OlafAPI\App_Data\Multimedia\" + ToPatch;
            if (File.Exists(destPath))
            {
                File.Delete(destPath);
            }
            String directoryPath = @"D:\Git\mgrOlaf\olafBackend\OlafAPI\App_Data\Multimedia\"+ ToPatch.Remove(ToPatch.LastIndexOf('\\'));
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            //File.Copy(sourcePath, destPath);
        }

        private static void AddSubcategories()
        {
            var categoris = new List<Category>();
            var categoryId = 0;
            using (var context = new OlafContext())
            {
                //categoris.AddRange(context.Categories.ToList());
                var tempCategories = context.Categories.Include("Name").ToList();

                categoryId = tempCategories.First().Id;
            }


            var activitis = new List<oldData.Activity>();
            using (var piLearnContext = new oldData.PILearnEntities())
            {
                activitis.AddRange(piLearnContext.Activity.ToList());
            }


            List<oldData.Person> persons = getPersons();
            //var oldPersons = new List<oldData.Person>();
            //using (var piLearnContext = new oldData.PILearnEntities())
            //{
            //    oldPersons.AddRange(piLearnContext.Person.ToList());
            //}
            var oldPersonCounter = 0;


            var wordTypeList = new List<WordType>();
            var wordTypeActivityList = new List<WordType>();
            var counter = 0;
            var tempCounterTest = 0;
            var categoryActionCounter = 0;
            var subactegoryActionCounter = 0;

            String fromPath;
            String toPath;
            var activitiesCounter = 1;

            foreach (var activity in activitis)
            {
                if (counter!= 0 && counter % 4 == 0 && counter != 40)
                {
                    categoryId += 1;
                }
                if (counter != 0 && counter % 4 == 0 && counter != 40)   //TODO: fix this
                {
                    oldPersonCounter += 1;
                }

                fromPath = String.Format(@"Audio\Activit\c{0}s{1}_FirstPerson.mp3", categoryActionCounter, subactegoryActionCounter);
                String firstPersonPath = String.Format(@"Audio\Activity\FirstPerson\{0}.mp3", activity.FirstPerson.Trim());
                //CoppyFile(fromPath, firstPersonPath);

                fromPath = String.Format(@"Audio\Activit\c{0}s{1}_Infinitive.mp3", categoryActionCounter, subactegoryActionCounter);
                String infinitivePath = String.Format(@"Audio\Activity\Infinitive\{0}.mp3", activity.FirstPerson.Trim());
                //CoppyFile(fromPath, infinitivePath);

                fromPath = String.Format(@"Audio\Activit\c{0}s{1}.mp3", categoryActionCounter, subactegoryActionCounter);
                String secondPath = String.Format(@"Audio\Activity\SecondPerson\{0}.mp3", activity.SecondPerson.Trim());
                //CoppyFile(fromPath, secondPath);

                fromPath = String.Format(@"Audio\Activit\c{0}s{1}_ThirdPerson.mp3", categoryActionCounter, subactegoryActionCounter);
                String thirdPersonPath = String.Format(@"Audio\Activity\ThirdPerson\{0}.mp3", activity.FirstPerson.Trim());
                //CoppyFile(fromPath, thirdPersonPath);


                var infinity = new Variance() { Text = activity.Infinitive, Sound = new Sound() { Path = StripText(infinitivePath) } };
                var secondPerson = new Variance() { Text = activity.SecondPerson, Sound = new Sound() { Path = StripText(secondPath) } };
                var firstPerson = new Variance() { Text = activity.FirstPerson, Sound = new Sound() { Path = StripText(firstPersonPath) } };
                var thirdPerson = new Variance() { Text = activity.ThirdPerson, Sound = new Sound() { Path = StripText(thirdPersonPath) } };

                var subcategory = new Subcategory();
                subcategory.Variance = infinity;
                subcategory.CategoryId = categoryId;

                if (subactegoryActionCounter > 1)
                {
                    tempCounterTest = 1;
                }
                else
                {
                    tempCounterTest = 0;
                }



                fromPath = String.Format(@"Audio\Person\c{0}s{1}.mp3", categoryActionCounter, tempCounterTest);
                String personSound = String.Format(@"Audio\Person\c{0}s{1}.mp3", categoryActionCounter, tempCounterTest);
                CoppyFile(fromPath, personSound);

                fromPath = String.Format(@"Person\c{0}s{1}.jpg", categoryActionCounter, tempCounterTest);
                String presonPicture = String.Format(@"Picture\Person\c{0}s{1}.jpg", categoryActionCounter, tempCounterTest);
                CoppyFile(fromPath, presonPicture);

                var personName = persons.FirstOrDefault(x => x.Path.Trim() == String.Format("c{0}s{1}", categoryActionCounter, tempCounterTest));
                Console.WriteLine(categoryActionCounter.ToString());
                subcategory.Persons = new List<Person>()//TODO: fix person
                {
                    new Person()
                    {
                        Name = new Variance() {
                            Text = personName.Name,
                            Sound = new Sound() {Path = StripText(personSound) }

                        },
                        Picture = new Picture()
                        {
                            Path =  StripText(presonPicture)
                        }
                    }

                };

                if (subactegoryActionCounter > 2)
                {
                    subactegoryActionCounter = 0;
                    categoryActionCounter += 1;
                }
                else
                {
                    subactegoryActionCounter += 1;
                }

                fromPath = String.Format(@"Activitis\activity ({0}).jpg", activitiesCounter);
                String activitesPath = String.Format(@"Picture\Activities\{0}.jpg", infinity.Text);


                subcategory.Picture = new Picture()
                {
                    Path = StripText(activitesPath)
                };


                var wordTypeCategory = new WordType();
                wordTypeCategory.Subcategory = subcategory;
                wordTypeCategory.Picture = new Picture()
                {
                    Path = ""
                };

                var wordTypeActivity = new WordType();
                wordTypeActivity.Activity = new Activity()
                {
                    FirstPerson = firstPerson,
                    Infinitive = infinity,
                    SecondPerson = secondPerson,
                    ThierdPerson = thirdPerson
                };


                //CoppyFile(fromPath, activitesPath);
                wordTypeActivity.Picture = new Picture()
                {
                    Path = StripText(activitesPath)
                };

                activitiesCounter += 1;
                wordTypeList.Add(wordTypeCategory);
                wordTypeActivityList.Add(wordTypeActivity);
                counter += 1;
            }
            using (var context = new OlafContext())
            {
                context.WordTypes.AddRange(wordTypeList);
                context.SaveChanges();
            }
            var subcategoryId = 0;
            using (var context = new OlafContext())
            {
                subcategoryId = context.Subcategories.First().Id;
            }
            foreach (var item in wordTypeActivityList)
            {
                item.SubcategoryId = subcategoryId;
                subcategoryId += 1;
            }

            using (var context = new OlafContext())
            {
                context.WordTypes.AddRange(wordTypeActivityList);
                context.SaveChanges();
            }
        }

        private static string StripText(string title)
        {
            string input = title.Trim().ToLower();
            string tmp = input.Replace("ą", "a");
            tmp = tmp.Replace("ć", "c");
            tmp = tmp.Replace("ę", "e");
            tmp = tmp.Replace("ł", "l");
            tmp = tmp.Replace("ń", "n");
            tmp = tmp.Replace("ó", "o");
            tmp = tmp.Replace("ś", "s");
            tmp = tmp.Replace("ź", "z");
            tmp = tmp.Replace("ż", "z");
            return tmp;
        }


        private static List<oldData.Person> getPersons()
        {
            var newPersons = new List<oldData.Person>();
            var listOfPersons = new List<string>() {
                "Ola",
                "Adam",
                "Mama",
                "Tata",
                "Pani",
                "Pan",
                "Siostra",
                "Brat",
                "Babcia",
                "Dziadek",
                "Mama",
                "Tata",
                "Ola",
                "Adam",
                "Siostra",
                "Brat",
                "Pani",
                "Pan",
                "Babica",
                "Dziadek"
            };
            var counter = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    var person = new oldData.Person();
                    person.Name = listOfPersons[counter];
                    person.Path = String.Format("c{0}s{1}", i, j);
                    newPersons.Add(person);
                        counter++;
                }
            }
            return newPersons;
        }

        private static List<int> AddCategories()
        {
            var ImagesPath = @"http://shmodel.pl/server/files/Categories/Category";
            var categoriesNames = new List<string>()
            {
                "Zajęcia z funkcjonowana w środowisku",
                "Ubrania",
                "Srodki transportu",
                "Zajęcia muzyczne i plastyczne",
                "Śniadanie i drugie śniadanie",
                "Obiad i deser",
                "Łazienka",
                "Pokój",
                "Kolacja",
                "Urządzenia"
            };
            var Ids = CreateVariance(categoriesNames);

            var listOfCategories = new List<Category>();
            for (int i = 0; i < 10; i++)
            {
                listOfCategories.Add(new Category() { NameId = Ids[i], Picture = new Picture() { Path = StripText(ImagesPath + i + ".jpg") } });
            }
            using (var context = new OlafContext())
            {
                context.Categories.AddRange(listOfCategories);
                context.SaveChanges();
            }
            return listOfCategories.Select(x => x.Id).ToList();
        }

        private static List<int> CreateVariance(List<string> names)
        {
            var listOfVariance = new List<Variance>();
            var audiPatch = @"http://shmodel.pl/server/files/Categories/Category";
            var counter = 0;
            foreach (var item in names)
            {
                listOfVariance.Add(new Variance()
                {
                    Sound = new Sound() { Path = StripText(audiPatch + counter + ".wav") },
                    Text = item,
                });
                counter++;
            }

            using (var context = new OlafContext())
            {
                context.Variances.AddRange(listOfVariance);
                context.SaveChanges();
            }
            return listOfVariance.Select(x => x.Id).ToList();
        }

     
    }
}
