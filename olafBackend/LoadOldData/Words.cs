//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LoadOldData
{
    using System;
    using System.Collections.Generic;
    
    public partial class Words
    {
        public int Id { get; set; }
        public string Path { get; set; }
        public Nullable<int> ActivityId { get; set; }
        public Nullable<int> NounId { get; set; }
    }
}
